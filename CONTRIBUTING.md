# Contributing to this project

> We try to make it as welcoming and _free_ as we can.

[[_TOC_]]

The project's development lives on GitLab; a website that hosts a version control system for files (called git). In GitLab you can keep track of tasks and objectives by opening _issues_. We use this system to organize the development.

Get in touch:

- OLA's Discord server: <https://discord.gg/QkRQp2b5Mr>
- GitLab issue board: <https://gitlab.com/pipettin-bot/pipettin-bot/-/boards/5715235>

Where to find things to do:

**Development**:

- There are GitLab issues at [the main board](https://gitlab.com/pipettin-bot/pipettin-bot/-/boards/5715235) on different aspects of the project, which need attention and discussion.
- If you want to open a discussion about something interesting to you, please post a new issue at the [contributions and support board](https://gitlab.com/pipettin-bot/pipettin-bot/-/boards/5026063) or drop a comment on our Discord (invite link above).
- Pull-requests are also very welcome! be it in README contents, code, an 3D models.

**Documentation**:

- Comment on issues, open new issues, feel at home!

You'll find the project's _issues_, tagged with colored _labels_, and arranged in an _issue board_ like this one:

[![issue_boards.svg](./doc/media/images/issue_boards.svg)](https://gitlab.com/pipettin-bot/pipettin-bot/-/boards/5715235)

## Roadmap

Overall:

- [x] Version 1: _Do it together_
  - GRBL firmware.
  - Express.js GUI.
  - Gilson micropipettes.
- [x] Version 2 (_WORK IN PROGRESS_): _Make it Pro_
  - [x] Klipper firmware.
  - [x] ReactJS GUI.
  - [x] Custom micropipettes.
  - [x] Basic plugin system.
  - [x] First labware integrations.
- [x] Version 3: _Make it Interoperable_
  - [x] PyLabRobot API integration.

## Open Lab automation landscape

Have a look at out "list" and "interaction" issue labels:

- Map conceptualization issue: <https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/96>
- Lists: <https://gitlab.com/pipettin-bot/pipettin-bot/-/issues?label_name%5B%5D=lists>

![automation_overview.svg](./doc/media/images/automation_overview.svg)
