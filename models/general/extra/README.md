# Extra reference models

NEMA 17 model for reference:

https://grabcad.com/library/e3d-high-torque-nema-17-stepper-motor-mt-1704hsm168re-1

![image.png](./images/image.png)

Models relevant to V-wheels: [v_wheel_models.FCDtd](./v_wheel_models.FCDtd)

![eccentric.png](./images/eccentric.png)

[p20_stalk_profile.FCStd](./p20_stalk_profile.FCStd)

![p20_stalk_profile-Body.png](./images/p20_stalk_profile-Body.png)

[p1000_stalk_profile.FCStd](./p1000_stalk_profile.FCStd)

![p1000_stalk_profile-Body.png](./images/p1000_stalk_profile-Body.png)
![p1000_tip_interior.png](./images/p1000_tip_interior.png)

