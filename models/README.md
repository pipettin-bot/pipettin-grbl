# 3D Models directory

Find herein 3D all printable models to make our CNC frame and exchangeable tool adapter for the pipetting robot.

Hardware is distributed under the terms of the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-bot/-/blob/master/LICENSES.md#hardware) licence.

- Most version 3 models have been migrated to a unified directory over here: [MK3](./MK3)
- General and reference models are in the [general](./general) directory.
- The [labware](./labware) directory contains a few draft models for basic lab equipment.
