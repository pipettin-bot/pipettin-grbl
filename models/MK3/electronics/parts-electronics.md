# Electronics

Download a zip file containing all the [model files](electronics-models.zip){zip, pattern:"electronics/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [Wire stripper]{qty: 1, cat: tool}
- [Dupont crimper]{qty: 1, cat: tool}

Materials:

- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}
- [Electrical wire]{qty: 1, cat: stock}
- [Electronics wire]{qty: 1, cat: stock}
- [Dupont crimp terminals]{qty: 2, cat: stock}
- [Heat shrink]{qty: 1, cat: stock}
- [Soldering iron]{qty: 1, cat: stock}
- [Heat gun]{qty: 1, cat: stock}

## Parts

Most of the electronics are mounted to the backpanel:

- Main board:
  - [Raspberry Pi]{qty: 1, cat: electronics} (model B, version 4 or greater).
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 4, cat: part_fastener}
  - [Step down buck-converter (3A)](../parts.yaml#lm2596-dc-dc-buck-converter){qty: 1, cat: electronics}.
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
  - [Step down converter PSU cable]{qty: 1, cat: electronics}.
  - [Step down converter RPi cable]{qty: 1, cat: electronics}.
- CNC controller board alternatives:
  - [Duet 2 WiFi]{qty: 1, cat: electronics}.
    - This board uses different wiring for the limit switches.
    - [M4 x 16mm Screw](../parts.yaml#m4-screw-16mm){qty: 4, cat: part_fastener}
  - [RAMPS board]{qty: 1, cat: electronics} (with [modifications for 24V](https://reprap.org/wiki/RAMPS_24v)).
  - Two [Arduino CNC-shield]{qty: 2, cat: electronics} boards.
    - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 8, cat: part_fastener}
  - Or any board with 5+ axes that [Klipper](https://www.klipper3d.org/) can drive.
  - Fixtures:
    - [Separator ID4 10 mm]{qty: 12, cat: man_3d} ([source](SEP-ALL.FCStd)/[SEP-4-10.V1.stl](exported_models/SEP-4-10.stl){previewpage}).
    - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 12, cat: part_fastener}
- Power supply:
  - [Power supply (24V 10A)]{qty: 1, cat: electronics}.
  - [IEC power connector with rocker switch](../parts.yaml#power-connector-iec-switch){qty: 1, cat: electronics}.
  - [Power fuse (10A)]{qty: 3, cat: electronics}.
  - [3D-printed PSU case]{qty: 1, cat: man_3d} ([source](PSU-BASE-IEC_SWITCH.FCStd)), one of the following:
    - Default: [PSU-IEC-C14-COVER-42MM](exported_models/PSU-IEC-C14-COVER-42MM.stl){previewpage}
    - Centered mounting points: [PSU-IEC-C14-COVER.stl](exported_models/PSU-IEC-C14-COVER.stl)
    - Taller PSU: [PSU-IEC-C14-COVER-50MM](exported_models/PSU-IEC-C14-COVER.stl)
  - [3D-printed PSU cable plate]{qty: 1, cat: man_3d} ([source](PSU-BASE-IEC_SWITCH.FCStd)/[PSU-OUTPUT-PLATE.stl](exported_models/PSU-OUTPUT-PLATE.stl)).
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 6, cat: part_fastener}
  - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 10, cat: part_fastener}
  - [M3x3 heat-set insert](../parts.yaml#m3x3-heatset-insert){qty: 4, cat: part_fastener}
  - [M4 x 10mm Screw](../parts.yaml#m4-screw-10mm){qty: 2, cat: part_fastener}
  - [M4 Hex Nut](../parts.yaml#m4-nut-hex){qty: 2, cat: part_fastener}
  - [M4x4 heat-set insert](../parts.yaml#m4x4-heatset-insert){qty: 2, cat: part_fastener}
  - Pair of holder clamps:
    - [3D-printed PSU clamps]{qty: 2, cat: man_3d} ([source](PSU-BASE-IEC_SWITCH.FCStd) / either [PSU-U-CLAMP-42MM.stl](exported_models/PSU-U-CLAMP-42MM.stl) or ).
    - [M3 x 12mm Screw](../parts.yaml#m3-screw-12mm){qty: 4, cat: part_fastener}
    - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
- Status display:
  - [OLED display module (0.96in I2C)](../parts.yaml#display-oled-module-I2C-0.96){qty: 1, cat: electronics}
  - Printed display base (**PLACEHOLDER**)
  - [M4 Drop-In Nut 2020](../parts.yaml#m4-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - M2 screws and nuts (**PLACEHOLDER**)
  - [OLED display cable]{qty: 1, cat: electronics} (**PLACEHOLDER**)
