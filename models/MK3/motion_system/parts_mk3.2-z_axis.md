# Motion system Z-axis

Parts for the Z-axis of the motion system, made from aluminum parts milled in a CNC machine.

Download a zip file containing all the [model files](z-axis-models.zip){zip, pattern:"motion_system/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Router](../parts.yaml#cnc-router){qty: 1, cat: tool}
- [M3 Tap](../parts.yaml#m3-tap){qty: 1, cat: tool}
- [M4 Tap](../parts.yaml#m4-tap){qty: 1, cat: tool}

Materials:

- [8mm Aluminum Sheet](../parts.yaml#aluminum-8mm){qty: 1, cat: stock}
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Parts

- Axis guides:
  - [M5 x 25mm Screw](../parts.yaml#m5-screw-25mm){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 2}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 2}
- Attachment to X-axis carriage:
  - [M4 x 20mm Screw](../parts.yaml#m4-screw-20mm){qty: 2, cat: part_fastener}
- Z-axis actuator:
  - [Z actuator aluminum base]{qty: 1, cat: man_mill} ([source](CAR-Z-BELT.FCStd)/[Z-BASE.step](exported_models/Z-BASE.step){previewpage})
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 5, cat: part_fastener}
  - [Z stepper base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[Z-ST-BASE-A.stl](exported_models/Z-ST-BASE-A.stl){previewpage})
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
  - [Z pulley base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[Z-ST-BASE-B-endstop.stl](exported_models/Z-ST-BASE-B-endstop.stl){previewpage})
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 5, cat: part_fastener}
  - Pulleys, wheels, and axles:
    - Belt guides:
      - [M5 Screw 50mm](../parts.yaml#m5-screw-50mm){qty: 2, cat: part_fastener}
      - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
      - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
    - Main pulley:
      - [GT2 Belt 6x600mm](../parts.yaml#gt2-6x600mm-belt){qty: 1}
      - [5x75mm Steel Rod](../parts.yaml#steel-rod-5x75mm){qty: 1}
      - [625ZZ Bearing](../parts.yaml#bearing-625zz){qty: 2}
      - [M3 x 8mm Screw](../parts.yaml#m3-screw-8mm){qty: 4, cat: part_fastener}
      - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
      - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
    - Reduction pulleys (1:5):
      - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
      - [GT2 80T Pulley](../parts.yaml#gt2-80T-pulley-ID5){qty: 1}
      - [GT2 Belt 6x200mm Closed](../parts.yaml#gt2-6x200mm-closed-belt){qty: 1}
      - [M5 Washer](../parts.yaml#m5-washer){qty: 4, cat: part_fastener}
- Z-axis motor:
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [NEMA 17 Cable 1000mm](../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
- Z-axis profile:
  - [2040 V-Slot Profile 470mm](../parts.yaml#2040-v-slot-aluminum-470mm){qty: 1}, M3 tapped near the end of one of the "40" slots.
  - Top GT2 belt mount:
    - [M5 x 16mm Screw](../parts.yaml#m5-screw-16mm){qty: 2, cat: part_fastener}
    - [Z belt top base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[CAR-Z-BELT-MOUNT.V2.stl](exported_models/CAR-Z-BELT-MOUNT.V2.stl){previewpage})
    - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener} (note: for the tool-changer cable guide base).
    - [Zip Ties](../parts.yaml#zip-tie){qty: 1}
    - [Top-side GT2 belt lock]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[GT2-LOCK-SLOT-V2.stl](exported_models/GT2-LOCK-SLOT-V2.stl){previewpage})
    - [Top-side GT2 belt lock lid]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[GT2-LOCK-SLOT-V2-LID.stl](exported_models/GT2-LOCK-SLOT-V2-LID.stl){previewpage})
  - Bottom GT2 belt mount:
    - [Z belt bottom clip]{qty: 1, cat: man_3d} ([source](Z-GT2-LOCK.FCStd)/[GT2-LOCK-2020-SLIDE.stl](exported_models/GT2-LOCK-2020-SLIDE.stl){previewpage})
    - [M3 x 10mm Conical Head Screw](../parts.yaml#m3-screw-10mm-conical-head){qty: 1, cat: part_fastener}
- Limit-switch:
  - [MakerBot Endstop Lever Z]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/[endstop-pivot-trigger.stl](exported_models/endstop-pivot-trigger.stl){previewpage})
  - [MakerBot Endstop Cover Z]{qty: 1, cat: man_3d} ([source](ENDSTOP-BASE.FCStd)/[ENDSTOP-COVER-Z-V3.stl](exported_models/ENDSTOP-COVER-Z-V3.stl){previewpage})
  - [M3 Separators 6.5mm]{qty: 2, cat: man_3d} ([source](ENDSTOP-BASE.FCStd)/[SEP_M3_L6.5.stl](exported_models/SEP_M3_L6.5.stl){previewpage})
  - [M3 x 25mm Screw](../parts.yaml#m3-screw-25mm){qty: 2, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
  - [M3 Washer](../parts.yaml#m3-washer){qty: 2, cat: part_fastener}
  - [M4 Drop-In Nut 2020](../parts.yaml#m4-drop-in-nut-2020){qty: 1, cat: part_fastener}
  - [M4 x 6mm Set screw](../parts.yaml#m4-set-screw-6mm){qty: 1, cat: part_fastener}
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
- Cable management:
  - [Base for Z cables]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/ [XZ-CABLE-BASE-x2-TILT.stl](exported_models/XZ-CABLE-BASE-x2-TILT.stl){previewpage})
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
