# Motion system Y-axis

Parts for the Y-axis of the motion system, made from aluminum parts milled in a CNC machine.

Download a zip file containing all the [model files](y-axis-models.zip){zip, pattern:"motion_system/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Router](../parts.yaml#cnc-router){qty: 1, cat: tool}
- [M3 Tap](../parts.yaml#m3-tap){qty: 1, cat: tool}

Materials:

- [8mm Aluminum Sheet](../parts.yaml#aluminum-8mm){qty: 1, cat: stock}
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Parts

- Y-axis stepper:
  - [Y Stepper Base V1]{qty: 1, cat: man_3d} ([source](Y-STEPPER-BASE.FCStd)/[Y-STEPPER-BASE.V2.stl](exported_models/Y-STEPPER-BASE.V2.stl){previewpage})
  - [M3 x 12mm Screw](../parts.yaml#m3-screw-12mm){qty: 4, cat: part_fastener}
  - [M4 Drop-In Nut 2020](../parts.yaml#m4-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M4 x 12mm Screw](../parts.yaml#m4-screw-12mm){qty: 2, cat: part_fastener}
  - [M3 Washer](../parts.yaml#m3-washer){qty: 8, cat: part_fastener}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
  - [NEMA 17 Double-Sided Stepper Motor](../parts.yaml#nema17-double-sided){qty: 1, cat: electronics}
  - [NEMA 17 5-5 Shaft Coupler](../parts.yaml#nema17-5-5-shaft-coupler){qty: 1}
- Y-axis right pulley:
  - [5x545mm Steel Rod](../parts.yaml#steel-rod-5x545mm){qty: 1}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
  - [Y Pulley Base V1]{qty: 1, cat: man_3d} ([source](Y-PULLEY-BASE.FCStd)/[Y-PULLEY-BASE.V2.stl](exported_models/Y-PULLEY-BASE.V2.stl){previewpage})
  - [M4 Drop-In Nut 2020](../parts.yaml#m4-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M4 x 12mm Screw](../parts.yaml#m4-screw-12mm){qty: 2, cat: part_fastener}
  - [Y Pulley Bearing Housing]{qty: 1, cat: man_3d} ([source](Y-PULLEY-BASE.FCStd)/[BEARING-HOUSING.stl](exported_models/BEARING-HOUSING.stl){previewpage})
  - [625ZZ Bearing](../parts.yaml#bearing-625zz){qty: 1}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
- Front belt idlers:
  - [GT2 16T Idler Pulley](../parts.yaml#gt2-pulley-idler-16t-3mm-shaft){qty: 2}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
- Y-axis carriages:
  - [Y-axis Carriage V3]{qty: 2, cat: man_mill} ([source](CAR-Y.FCStd)/[CAR-Y.V3.step](exported_models/CAR-Y.V3.step))
  - [Zip Ties](../parts.yaml#zip-tie){qty: 4}
  - [GT2 Belt 6x1000mm](../parts.yaml#gt2-belt-6x1000mm){qty: 2}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 8}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 4}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 4}
  - [M5 x 30mm Screw](../parts.yaml#m5-screw-30mm){qty: 8, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 8, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 8, cat: part_fastener}
- Belt clamps:
  - [GT2 Hairpin]{qty: 4, cat: man_3d} ([source](CAR-Y.FCStd)/[GT2-HAIRPIN-rev2.stl](exported_models/GT2-HAIRPIN-rev2.stl){previewpage})
- Attachment to X-axis gantry:
  - [GT2 Lock Grooved]{qty: 4, cat: man_3d} ([source](X-GT2-LOCK.FCStd)/[X-GT2-LOCK-GROOVED-12mm-tol.stl](exported_models/X-GT2-LOCK-GROOVED-12mm-tol.stl){previewpage})
  - [M3 x 12mm Screw](../parts.yaml#m3-screw-12mm){qty: 4, cat: part_fastener}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 6, cat: part_fastener}
  - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 6, cat: part_fastener}
- Electronics and wiring:
  - [500mm NEMA 17 Cable](../parts.yaml#nema17-cable-500mm){qty: 1, cat: electronics}
  - [MakerBot Endstop Base Y]{qty: 1, cat: man_3d} ([source](ENDSTOP-BASE.FCStd)/[ENDSTOP-BASE-2020-V3-top.stl](exported_models/ENDSTOP-BASE-2020-V3-top.stl){previewpage})
  - [MakerBot Endstop Base Y Cover]{qty: 1, cat: man_3d} ([source](ENDSTOP-BASE.FCStd)/[ENDSTOP-BASE-2020-V3-cover.stl](exported_models/ENDSTOP-BASE-2020-V3-cover.stl){previewpage})
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 2, cat: part_fastener}
  - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 2, cat: part_fastener}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
