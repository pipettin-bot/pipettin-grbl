# Motion system

Parts for the motion system.

This version drives the Z-axis with a timing belt, in order to reduce vertical space, making it more portable.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools, any of:

- [CNC Laser Cutter](../parts.yaml#cnc-laser-cutter){qty: 1, cat: tool}
- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Router](../parts.yaml#cnc-router){qty: 1, cat: tool}

Materials, any of:

- [8mm Aluminum Sheet](../parts.yaml#aluminum-8mm){qty: 1, cat: stock} **PLACEHOLDER**
- [8mm Acrylic Sheet](../parts.yaml#acrylic-8mm){qty: 1, cat: stock} **PLACEHOLDER**
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Y-axis {pagestep}

Parts:

- Y-axis stepper:
  - [Y Stepper Base V1]{qty: 1, cat: man} ([source](Y-STEPPER-BASE.FCStd)/Y-STEPPER-BASE.V1)
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M3 x 12mm Screw](../parts.yaml#m3-screw-12mm){qty: 6, cat: part_fastener}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
  - [NEMA 17 Double-Sided Stepper Motor](../parts.yaml#nema17-double-sided){qty: 1, cat: electronics}
- Y-axis right pulley:
  - [Y Pulley Base V1]{qty: 1, cat: man} ([source](Y-PULLEY-BASE.FCStd)/Y-PULLEY-BASE.V1)
  - [Y Pulley Bearing Housing]{qty: 1, cat: man_3d} ([source](Y-PULLEY-BASE.FCStd)/BEARING-HOUSING)
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [625ZZ Bearing](../parts.yaml#bearing-625zz){qty: 1}
  - [5x545mm Steel Rod](../parts.yaml#steel-rod-5x545mm){qty: 1}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
- Y-axis carriages:
  - [Y-axis Carriage V2]{qty: 2, cat: man} ([source](CAR-Y.FCStd)/CAR-Y.V3.1-rev2)
  - [Zip Ties](../parts.yaml#zip-tie){qty: 4}
  - [GT2 Belt 6x1000mm](../parts.yaml#gt2-belt-6x1000mm){qty: 2}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 8}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 4}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 4}
  - [M5 x 30mm Screw](../parts.yaml#m5-screw-30mm){qty: 8, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 8, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 8, cat: part_fastener}
- Front idlers:
  - [GT2 16T Idler Pulley](../parts.yaml#gt2-pulley-idler-16t-3mm-shaft){qty: 2}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
- Electronics and wiring:
  - [500mm NEMA 17 Cable](../parts.yaml#nema17-cable-500mm){qty: 2, cat: electronics}
  - [MakerBot Endstop Base V2]{qty: 1, cat: man_3d} ([source](ENDSTOP-BASE.FCStd)/ENDSTOP-BASE-2020-V2)
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}

Outputs:

- [y-axis]{output, qty:1}

## X-axis {pagestep}

- X-axis carriage:
  - Double-sided, for the plastic version:
    - [Carriage Front v1.1]{qty: 1, cat: man} ([source](CAR-X-v1.1.FCStd)/CAR-X-FRONT-v1.1)
    - [Carriage Back v1.1]{qty: 1, cat: man} ([source](CAR-X-v1.1.FCStd)/CAR-X-BACK-v1.1)
  - Single-sided, for the aluminum version:
    - [Aluminum X Carriage]{qty: 1, cat: man} ([source](CAR-X-v1.1.FCStd)/CAR-X-v1.3)
- Electronics and wiring:
  - [MakerBot Endstop Base]{qty: 1, cat: man_3d} ([source](CAR-X-v1.1.FCStd)/MAKERBOT-ENDSTOP-BASE)
  - [Microswitch Base]{qty: 1, cat: man_3d} ([source](CAR-X-v1.1.FCStd)/MICROSWITCH-BASE)
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
  - [NEMA 17 Cable 1000mm](../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
- X-axis guide:
  - [2040 V-Slot Aluminum 596mm](../parts.yaml#2040-v-slot-aluminum-596mm){qty: 1}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 4}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 8}
  - [M5 Screw 50mm](../parts.yaml#m5-screw-50mm){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 8, cat: part_fastener}
- Attachment to Y-axis carriages:
  - [GT2 Lock Grooved]{qty: 2, cat: man_3d} ([source](X-GT2-LOCK.FCStd)/X-GT2-LOCK-GROOVED-12mm)
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 6, cat: part_fastener}
  - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 6, cat: part_fastener}
- Motor and transmission:
  - [GT2 6x800mm Belt](../parts.yaml#gt2-belt-6x800mm){qty: 1}
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}

Outputs:

- [x-axis]{output, qty:1}

## Z-axis {pagestep}

- Axis guides:
  - [M5 x 30mm Screw](../parts.yaml#m5-screw-30mm){qty: 4, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 2}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 2}
- Attachment to X-axis carriage:
  - [M4 Nut Square](../parts.yaml#m4-nut-square){qty: 4, cat: part_fastener}
  - [M4 x 20mm Screw](../parts.yaml#m4-screw-20mm){qty: 4, cat: part_fastener}
- Z-axis actuator:
  - [Z actuator base]{qty: 1, cat: man} ([source](CAR-Z-BELT.FCStd)/Z-BASE)
  - [Z stepper base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/Z-ST-BASE-A)
  - [Z pulley base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/Z-ST-BASE-B)
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 2}
  - [GT2 80T Pulley](../parts.yaml#gt2-80T-pulley-ID5){qty: 1}
  - [GT2 Belt 6x500mm](../parts.yaml#gt2-6x500mm-belt){qty: 1}
  - [GT2 Belt 6x200mm Closed](../parts.yaml#gt2-6x200mm-closed-belt){qty: 1}
  - [625ZZ Bearing](../parts.yaml#bearing-625zz){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
- Z-axis motor:
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [NEMA 17 Cable 1000mm](../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
- Z-axis profile:
  - [2040 V-Slot Profile 400mm](../parts.yaml#2040-v-slot-aluminum-400mm){qty: 1}
  - Top GT2 belt mount:
    - [Z belt top base]{qty: 1, cat: man_3d} ([source](CAR-Z-BELT.FCStd)/CAR-Z-BELT-MOUNT.V2)
    - [M5 x 16mm Screw](../parts.yaml#m5-screw-16mm){qty: 2, cat: part_fastener}
    - [M5 Washer](../parts.yaml#m5-washer){qty: 2, cat: part_fastener}
- Limit-switch:
  - [Microswitch Endstop](../parts.yaml#microswitch-endstop){qty: 1}
  - [M3 x 16mm Conical Head Screw](../parts.yaml#m3-screw-16mm-conical-head){qty: 1, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 1, cat: part_fastener}
  - [Microswitch Cable 1000mm](../parts.yaml#microswitch-cable-1000mm){qty: 1}

Outputs:

- [z-axis]{output, qty:1}
