# Motion system

Here is the complete [Bill of materials]{BOM} for this guide.

## Motion system parts

Tools, any of:

- [CNC Laser Cutter](../parts.yaml#cnc-laser-cutter){qty: 1, cat: tool}
- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Router](../parts.yaml#cnc-router){qty: 1, cat: tool}

Materials, any of:

- [8mm Aluminum Sheet](../parts.yaml#aluminum-8mm){qty: 1, cat: stock} **PLACEHOLDER**
- [8mm Acrylic Sheet](../parts.yaml#acrylic-8mm){qty: 1, cat: stock} **PLACEHOLDER**
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Y-axis {pagestep}

Parts:

- [Y Stepper Base V1]{qty: 1, cat: man}: [source](Y-STEPPER-BASE.FCStd)
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M3 x 12mm Screw](../parts.yaml#m3-screw-12mm){qty: 6, cat: part_fastener}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
  - [NEMA 17 Double-Sided Stepper Motor](../parts.yaml#nema17-double-sided){qty: 1, cat: electronics}
- [Y Pulley Base V1]{qty: 1, cat: man}: [source](Y-PULLEY-BASE.FCStd)
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [625ZZ Bearing](../parts.yaml#bearing-625zz){qty: 1}
  - [5x545mm Steel Rod](../parts.yaml#steel-rod-5x545mm){qty: 1}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
- [Y-axis Carriage V2]{qty: 2, cat: man}: [source](CAR-Y.FCStd)
  - [Zip Ties](../parts.yaml#zip-tie){qty: 4}
  - [GT2 Belt 6x1000mm](../parts.yaml#gt2-belt-6x1000mm){qty: 2}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 8}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 4}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 4}
  - [M5 x 30mm Screw](../parts.yaml#m5-screw-30mm){qty: 8, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 8, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 8, cat: part_fastener}
- Front idlers:
  - [GT2 16T Idler Pulley](../parts.yaml#gt2-pulley-idler-16t-3mm-shaft){qty: 2}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 2, cat: part_fastener}
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
- Electronics and wiring:
  - [500mm NEMA 17 Cable](../parts.yaml#nema17-cable-500mm){qty: 2, cat: electronics}
  - [Endstop Base 2020 V2]{qty: 1, cat: man_3d}: [source](ENDSTOP-BASE.FCStd)
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}

Outputs:

- [y-axis]{output, qty:1}

## X-axis {pagestep}

- Carriage: [source](CAR-X-v1.1.FCStd)
  - [Carriage Front v1.1]{qty: 1, cat: man}
  - [Carriage Back v1.1]{qty: 1, cat: man}
- Electronics and wiring:
  - [MakerBot Endstop Base]{qty: 1, cat: man_3d}: [source](CAR-X-v1.1.FCStd)
  - [Microswitch Base]{qty: 1, cat: man_3d}: [source](CAR-X-v1.1.FCStd)
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
  - [NEMA 17 Cable 1000mm](../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
- Axis guide:
  - [2040 V-Slot Aluminum 596mm](../parts.yaml#2040-v-slot-aluminum-596mm){qty: 1}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 4}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 8}
  - [M5 Screw 50mm](../parts.yaml#m5-screw-50mm){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 8, cat: part_fastener}
- Attachment to Y-axis carriages:
  - [GT2 Lock Grooved]{qty: 2, cat: man_3d}: [source](X-GT2-LOCK.FCStd)
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 6, cat: part_fastener}
  - [M3 x 20mm Screw](../parts.yaml#m3-screw-20mm){qty: 6, cat: part_fastener}
- Motor and transmission:
  - [GT2 6x800mm Belt](../parts.yaml#gt2-belt-6x800mm){qty: 1}
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}

## Z-axis {pagestep}

- Attachment to X-axis carriage:
  - [Z-Axis Attachment Part V4C]{qty: 1, cat: man}: [source](CAR-Z-ST.FCStd)
  - [M4 Nut Square](../parts.yaml#m4-nut-square){qty: 4, cat: part_fastener}
  - [M4 x 20mm Screw](../parts.yaml#m4-screw-20mm){qty: 4, cat: part_fastener}
- Wheels:
  - [M5 x 30mm Screw](../parts.yaml#m5-screw-30mm){qty: 4, cat: part_fastener}
  - [M5 Washer](../parts.yaml#m5-washer){qty: 4, cat: part_fastener}
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 2}
- Stepper:
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [M3 Hex Standoff 25mm](../parts.yaml#m3-hex-standoff-25mm){qty: 2}
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 2, cat: part_fastener}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
Load-cells:
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 4}
  - [Load Cells](../parts.yaml#load-cell){qty: 2} **PLACEHOLDER**
  - [M4 Screw 25mm](../parts.yaml#m4-screw-25mm){qty: 4, cat: part_fastener}
  - [M4 x 20mm Screw](../parts.yaml#m4-screw-20mm){qty: 2, cat: part_fastener}
  - [HX711 80SPS ADC Board](../parts.yaml#hx711-80sps-adc-board){qty: 2}
  - [Raspberry Pi Pico MCU](../parts.yaml#rpi-pico-mcu){qty: 1}
  - [Micro USB Cable 1m](../parts.yaml#micro-usb-cable-1m){qty: 1}
  - [Pico ADC Enclosure]{qty: 1, cat: man_3d} **PLACEHOLDER**
- Transmission:
  - [GT2 6x150mm Closed Belt](../parts.yaml#gt2-belt-closed-6x150mm){qty: 1}
  - [GT2 Pulley 20T ID8](../parts.yaml#gt2-pulley-20t-id8){qty: 2}
- Lead-screw.
  - [608ZZ Housing Flange]{qty: 1, cat: man}: [source](CAR-Z-ST.FCStd)
  - [608ZZ Bearing](../parts.yaml#bearing-608zz){qty: 1}
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
  - [M3 Washer](../parts.yaml#m3-washer){qty: 4, cat: part_fastener}
  - [THSL Leadscrew OD8 L400mm](../parts.yaml#thsl-leadscrew-OD8-L2-S1-400mm){qty: 1}
  - [THSL Anti-Backlash Nut D8xL2](../parts.yaml#thsl-nut-D8-L2-S1-AB){qty: 1}
- Z-axis profile:
  - [2040 V-Slot Profile 400mm](../parts.yaml#2040-v-slot-aluminum-400mm){qty: 1}
  - [Z-Axis Nut Mount V2]{qty: 1, cat: man}: [source](CAR-Z-NUT-MOUNT.FCStd)
    - [M5 x 16mm Screw](../parts.yaml#m5-screw-16mm){qty: 2, cat: part_fastener}
    - [M5 Washer](../parts.yaml#m5-washer){qty: 2, cat: part_fastener}
  - [Z-Axis TC3 Base V1]{qty: 1, cat: man_3d}: [source](CAR-Z-NUT-MOUNT.FCStd)
    - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
    - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
- Limit-switch:
  - [Microswitch Endstop](../parts.yaml#microswitch-endstop){qty: 1}
  - [M3 x 16mm Conical Head Screw](../parts.yaml#m3-screw-16mm-conical-head){qty: 1, cat: part_fastener} **PLACEHOLDER**
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 1, cat: part_fastener}
  - [Microswitch Cable 1000mm](../parts.yaml#microswitch-cable-1000mm){qty: 1}
