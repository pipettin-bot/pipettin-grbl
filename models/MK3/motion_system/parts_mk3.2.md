# Motion system

Parts for the motion system, made from aluminum parts milled in a CNC machine.

In this version, the mounting points of the end-stops have been given thought and modelled.

Here is the complete [Bill of materials]{BOM} for this guide. Download 3D-printable STL files and STEP models from each page below.

## Y-axis {pagestep}

Open the [.](./parts_mk3.2-y_axis.md){step} page.

## X-axis {pagestep}

Open the [.](./parts_mk3.2-x_axis.md){step} page.

## Z-axis {pagestep}

Open the [.](./parts_mk3.2-z_axis.md){step} page.
