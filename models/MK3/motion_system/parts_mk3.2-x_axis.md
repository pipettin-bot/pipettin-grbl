# Motion system X-axis

Parts for the X-axis of the motion system, made from aluminum parts milled in a CNC machine.

Download a zip file containing all the [model files](x-axis-models.zip){zip, pattern:"motion_system/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Router](../parts.yaml#cnc-router){qty: 1, cat: tool}

Materials:

- [8mm Aluminum Sheet](../parts.yaml#aluminum-8mm){qty: 1, cat: stock}
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Parts

- X-axis carriage:
  - Single-sided, for the aluminum version:
    - [X-axis Carriage v1.4C]{qty: 1, cat: man_mill} ([source](CAR-X-v1.1.FCStd)/[CAR-X-v1.4C.step](exported_models/CAR-X-v1.4C.step))
  - Double-sided, for the plastic version:
    - See [parts_mk3.1](parts_mk3.1.md).
- X-axis guide:
  - [2040 V-Slot Aluminum 596mm](../parts.yaml#2040-v-slot-aluminum-596mm){qty: 1} (cut to 596 mm).
  - [2020 V-Wheels](../parts.yaml#2020-v-wheels){qty: 2}
  - [2020 V-Wheels Smooth](../parts.yaml#2020-v-wheels-smooth){qty: 2}
  - [Eccentric Spacer ID5 L6](../parts.yaml#spacer-eccentric-id5-l6){qty: 2}
  - [Spacer ID5 L6](../parts.yaml#spacer-id5-l6){qty: 2}
  - [M5 Screw 30mm](../parts.yaml#m5-screw-30mm){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 4, cat: part_fastener}
- Motor and transmission:
  - [NEMA 17 Stepper Motor](../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [M3 Hex Standoff 25mm](../parts.yaml#m3-hex-standoff-25mm){qty: 2}
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
  - [GT2 16T Pulley](../parts.yaml#gt2-pulley-16t-id5){qty: 1}
  - [GT2 6x800mm Belt](../parts.yaml#gt2-belt-6x800mm){qty: 1}
- Electronics and wiring:
  - [NEMA 17 Cable 1000mm](../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
  - [MakerBot Endstop](../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
  - [MakerBot Endstop Cable 1000mm](../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
  - [MakerBot Endstop Base X]{qty: 1, cat: man_3d} ([source](CAR-X-v1.1.FCStd)/[MAKERBOT-ENDSTOP-BASE.stl](exported_models/MAKERBOT-ENDSTOP-BASE.stl){previewpage})
  - [M3 x 8mm Screw](../parts.yaml#m3-screw-8mm){qty: 2, cat: part_fastener}
  - [M3 x 10mm Screw](../parts.yaml#m3-screw-10mm){qty: 2, cat: part_fastener}
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
