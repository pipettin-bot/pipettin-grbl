These files are derived from the `board-mount-dynamic.FCStd` and `board-mount-static.FCStd` files from the LumenPnP project.

Distributed under the CERN-OHL-W v2 license. Full text is available at https://cern.ch/cern-ohl.
