# Baseplate

Download a zip file containing all the [model files](baseplate-models.zip){zip, pattern:"baseplate/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Base-plate parts

Tools:

- [CNC Laser Cutter](../parts.yaml#cnc-laser-cutter){qty: 1, cat: tool}

Parts:

- Acrylic stock: [650 x 400 x 8 mm Acrylic Sheet](../parts.yaml#acrylic-sheet-650x400x8){qty: 1, cat: stock}
- [M5 x 10 mm Button Head Screw](../parts.yaml#m5-screw-10mm-button-head){qty: 3, cat: part_fastener}
  - You can use shorter ones if the baseplate has threaded holes for these screws.

## Cut the baseplate {pagestep}

Cut the baseplate using 8 mm acryllic stock.

Files for cutting can be generated from the [FreeCAD model of the baseplate](baseplate.FCStd):

- DXF: [BASEPLATE-v3.dxf](exported_models/BASEPLATE-v3.dxf){previewpage}
- STEP: [BASEPLATE-v3.step](exported_models/BASEPLATE-v3.step){previewpage}

Instructions for laser cutting and generating DXF files are available in [our knowledgebase](https://docs.openlabautomata.xyz/Knowledgebase/CNC-Laser-Cutting/#on-your-pc-dxf-export).

## Add mounting screws {pagestep}

Screw 3 [M5 x 10 mm Button Head Screws](../parts.yaml#m5-screw-10mm-button-head) to the underside of the baseplate.

Secure them with M5 nuts if necessary from the top side.

## Print platform curbs

3D-printed alignment "curbs" for labware:

- [Line curbs]{qty: 4, cat: man_3d} ([source](curbs.FCStd)/[MOD-I.V1.stl](exported_models/MOD-I.V1.stl){previewpage})
- [Corner curbs]{qty: 4, cat: man_3d} ([source](curbs.FCStd)/[MOD-L.V2.stl](exported_models/MOD-L.V2.stl){previewpage})
