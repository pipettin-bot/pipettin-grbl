# Tool-changer

The tool-changer has a linear axis that remotely actuates the mechanism on the head plate.
These are connected by a cable, as found in bicycle brakes.

Here is the complete [Bill of materials]{BOM} for the guides included below.

## Remote actuator

Follow the [.](./remote/parts-remote.md){step} guide.

## Head-plate & Locking mechanism

Follow the [.](./head_plate/parts-head_plate.md){step} guide.
