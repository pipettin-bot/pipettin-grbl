# Head plate

Download a zip file containing all the [model files](tc-headplate-models.zip){zip, pattern:"toolchanger/head_plate/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../../parts.yaml#3d-printer){qty: 1, cat: tool}
- [Bench grinder](../../parts.yaml#bench-grinder){qty: 1, cat: tool}, to bevel the locking leadscrew.
- [M6 Tap](../../parts.yaml#m6-tap){qty: 1, cat: tool}, to tap the flanges on the KFL08 bearing.
- [M3 Tap](../../parts.yaml#m3-tap){qty: 1, cat: tool}, to tap a hole for supporting the lock's main spring.
- [Wire Cutters](../../parts.yaml#wire-cutters){qty: 1, cat: tool}, to cut the main spring and belts.
- [Pliers](../../parts.yaml#pliers){qty: 1, cat: tool}, to bend the main spring.
- [Superglue](../../parts.yaml#superglue){qty: 1, cat: tool}, to make loops in the ends of the locking GT2 belt.
- [Square Crimper Tool](../../parts.yaml#square-crimper-tool){qty: 1, cat: tool}, to make a loop on the remote's cable.
  - Nice read on crimping ferrules: <https://library.automationdirect.com/ferrule-ferrule-ferrule-right/>

Materials:

- [2040 V-Slot Profile 400mm](../../parts.yaml#2040-v-slot-aluminum-400mm) from the Z axis, part of the [motion system](../../motion_system/parts_mk3.1.md#z-axis-pagestep).
- [PLA Filament 1.75mm](../../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Parts

### Head-plate

Parts:

- KFL08 mount:
  - [KFL08 Pillow Block Bearing](../../parts.yaml#bearing-pillow-block-KFL08){qty: 1, cat: part_custom}: Front support bearing. Must be M6-tapped on the flanges manually.
  - [M6 x 8mm Screw](../../parts.yaml#m6-screw-8mm){qty: 2, cat: part_fastener}
- Kinematic coupling:
  - [Dowel Pin 4mm x 16mm](../../parts.yaml#dowel-pin-OD4-L16){qty: 6, cat: part_fastener}
- Z-profile fixture:
  - [M4 Drop-In Nut 2020](../../parts.yaml#m4-drop-in-nut-2020){qty: 4, cat: part_fastener}
  - [M4 x 8mm Screw](../../parts.yaml#m4-screw-8mm){qty: 4, cat: part_fastener}
- 3D-printed parts:
  - [KFL08 headplate]{qty: 1, cat: man_3d} ([source](HEADPLATE.FCStd)/[HEADPLATE-KFL08.stl](exported_models/HEADPLATE-KFL08.stl){previewpage})
    - Check your bearing's dimensions before printing, as some KFL08 bearings are slightly larger.
    - Here is a [bearing variant](../../parts.yaml#bearing-pillow-block-KFL08-alt) and a matching [HEADPLATE-KFL08-V2-alt.stl](exported_models/HEADPLATE-KFL08-V2-alt.stl)

### Locking mechanism

Parts:

- Custom parts:
  - [Beveled 8mm OD x 36mm Leadscrew](../../parts.yaml#thsl-leadscrew-OD8-L8-S4-P2-36mm){qty: 1, cat: part_custom}: must be beveled manually on one end ([THSL-8x4-SCREW-L36 reference model](exported_models/THSL-8x4-SCREW-L36.zip)).
- Base part:
  - [TC lock 608ZZ Bearing Base]{qty: 1, cat: man_3d} ([source](HEADPLATE-LOCK.FCStd)/[608ZZ-BASE.V3.stl](exported_models/608ZZ-BASE.V3.stl){previewpage})
  - [608ZZ Bearing](../../parts.yaml#bearing-608zz){qty: 1}: Back support bearing.
  - [M4 x 6mm Set screw](../../parts.yaml#m4-set-screw-6mm){qty: 2, cat: part_fastener}
  - [M4 Hex Nut](../../parts.yaml#m4-nut-hex){qty: 2, cat: part_fastener}
- Belt guide:<!-- TODO: There is a new version for the mini bearing base -->
  - [Mini GT2 Idler Bearing Base]{qty: 1, cat: man_3d} ([source](HEADPLATE-LOCK.FCStd)/[MINI-BEARING-BASE-V3.stl](exported_models/MINI-BEARING-BASE-V3.stl){previewpage})
  - [Mini GT2 Idler 12T L4]{qty: 2, cat: man_3d} ([source](HEADPLATE-LOCK.FCStd)/[MINI-GT2-IDLER-12T-L4-ID4.stl](exported_models/MINI-GT2-IDLER-12T-L4-ID4.stl){previewpage})
  - [4mm OD x 1.5mm ID Bearing](../../parts.yaml#tiny-bearing-OD4mm-ID1.5mm){qty: 4}: tiny bearings.
  - [14 mm axle (1.5mm OD)](../../parts.yaml#tiny-axle-14mm-OD1.5mm){qty: 2}: tiny axle for the tiny bearings.
    - You can instead use [small bootlace ferrules](../../parts.yaml#ferrule-ID1.3mm). Place them and remove the excess with wire cutters.
- Pulley:
  - [32-teeth GT2 Pulley with THSL 8x4 Bore]{qty: 1, cat: man_3d} ([source](GT2-PULLEY-THSL-BORE.FCStd)/[AS-GT2-PULLEY-THSL-BORE-Compound.stl](exported_models/AS-GT2-PULLEY-THSL-BORE-Compound.stl){previewpage})
  - [TC lock shim (8x23x2mm)]{qty: 1, cat: man_3d} ([source](GT2-PULLEY-THSL-BORE.FCStd)/[AS-PULLEY-STOP-Compound.stl](exported_models/AS-PULLEY-STOP-Compound.stl){previewpage})
- Anti belt-slip idler:
  - [625ZZ Bearing](../../parts.yaml#bearing-625zz){qty: 1}: To prevent the belt from slipping.
  - [625ZZ Bearing Base]{qty: 1, cat: man_3d} ([source](HEADPLATE-LOCK.FCStd)/[SEP-M3-L14-DO5.1-F2-OD8.stl](exported_models/SEP-M3-L14-DO5.1-F2-OD8.stl){previewpage})
  - [625ZZ Bearing Separator]{qty: 1, cat: man_3d} ([source](HEADPLATE-LOCK.FCStd)/[SEP-ID5.4-L8-DO8.stl](exported_models/SEP-ID5.4-L8-DO8.stl){previewpage})
  - [M3 x 25mm Screw](../../parts.yaml#m3-screw-25mm){qty: 1, cat: part_fastener}
  - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 1, cat: part_fastener}
- Cable guide base for the Z-axis:
  - [Cable guide Z base]{qty: 1, cat: man_3d} ([source](../../motion_system/CAR-Z-BELT.FCStd)/[CAR-Z-TC3-BASE.V1)](../../motion_system/exported_models/CAR-Z-TC3-BASE.V1.stl){previewpage})
  - [M3 x 25mm Screw](../../parts.yaml#m3-screw-25mm){qty: 2, cat: part_fastener}

The mini bearings and axle might be hard to source locally. Alternatively you can:

- Print `MINI-GT2-IDLER-12T-L4-ID1.5` ([source](HEADPLATE-LOCK.FCStd))
  - Add graphite powder to the insides to keep this lubricated.
- Replace the axles with 1.2 mm hypodermic needles and trim them to length.

### Spring loaded lock

Parts:

- [400mm OD 5mm ID Spring](../../parts.yaml#spring-ext-L400-CS0.5-OD5){qty: 1, cat: stock} **PLACEHOLDER**
- [M3 x 12mm Screw](../../parts.yaml#m3-screw-12mm){qty: 1, cat: part_fastener}, to fix the spring to the 2040 profile.
- [GT2 6x600mm Belt](../../parts.yaml#gt2-belt-6x600mm){qty: 1}, locking belt.
- [Square Ferrule 1.5mm ID](../../parts.yaml#ferrule-ID1.5mm){qty: 1}

### Tool plates

Each tool should design its plate based on this template:

- [TOOLPLATE-WEDGE-V2.stl](exported_models/TOOLPLATE-WEDGE-V2.stl) ([source](HEADPLATE.FCStd)).

All tools share the same wedge-plate.

- [THSL-WEDGE-PLATE-BEVEL-M3-COMP.stl](exported_models/THSL-WEDGE-PLATE-BEVEL-M3-COMP.stl)
- This should be printed in detail (0.2 mm layer).
- Test the fit with a [THSL Leadscrew OD8 L400mm](../../parts.yaml#thsl-leadscrew-OD8-L2-S1-400mm) leadscrew.
  - The leadscrew will smooth out imperfections in the print after a few passes.
  - It's ok if it requires some force at first and then softens.
  - However, reprint with XY-size compensation set to -0.1 mm if it's too tight.
