# Tool-changer variant 3

Using 3D-printed timing gear with THSL thread inside. The gear is sandwiched between the KFL08 (tool side) and a 608ZZ (back side), such that it will not slide, and instead will rotate the THSL leadscrew at the center.

A few extra parts address issues with this design:

- Small mini bearings (4 mm OD) will guide the GT2 belt to the timing gear. Otherwise the belt can get stuck on the edges of the 2040 profile.
- The separator at the bottom has a larger diameter, such that it is barely below the GT2 belt when it is turning around the timing gear. Hopefully this will keep the belt from "skipping teeth" during toolchanges.
  - An appropriately sized bearing would be better.

![tc3-refined.png](./images/tc3-refined.png)

## Notes

The ID of the mini bearings might be 1.5 mm.

### Printing

- 0.3 mm layers are fine.
- I prefer organic supports.
- Tall and thing parts should be at least two, otherwise they won't cool properly and distort.
- `COMP-THSL_8x4_SHORT-IDLER-10ID-L6`: The THSL-GT2 gear requires -0.15 XY compensation on PrusaSlicer.
- `MINI-BEARING-BASE`: Check that the outer perimeter is complete near the holes for the axles.

### KFL08

- The distance between mounting holes of the KFL08 is 36.5 according to the [reference drawings](../GENERAL-MODELS/KFL08.FCStd). This works.
  - The vendor claims the distance is 36: <https://ingia.com.ar/producto/soporte-varilla-roscada-thsl-8mm-kfl08/>
