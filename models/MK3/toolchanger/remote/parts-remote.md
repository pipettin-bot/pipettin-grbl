# Remote

Download a zip file containing all the [model files](tc-remote-models.zip){zip, pattern:"toolchanger/remote/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

- [3D Printer](../../parts.yaml#3d-printer){qty: 1, cat: tool}
- [Wire Cutters](../../parts.yaml#wire-cutters){qty: 1, cat: tool}
- [Drill]{qty: 1, cat: tool} and [3 mm drill bit]{qty: 1, cat: tool}, to drill mounting holes on the backpanel.
- [Angle grinder](../../parts.yaml#angle-grinder){qty: 1, cat: tool} to cut the steel rods to length.
- [Lithium Grease](../../parts.yaml#grease-lithium){qty: 1, cat: tool}

### Remote actuator

Parts:

- Cable and guide (a.k.a. bowden tube):
  - [1500mm Reinforced Bowden Tube](../../parts.yaml#1500m-reinforced-bowden-tube){qty: 1, cat: stock} **PLACEHOLDER**
  - [2000mm Steel Cable](../../parts.yaml#2000m-steel-cable){qty: 1, cat: stock} **PLACEHOLDER**
  - One of the cable guide bases for the structure side, from the [back-panel](../../backpanel/parts-backpanel.md) page.
- Mount to back-panel:
  - [M3 x 20mm Screw](../../parts.yaml#m3-screw-20mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener} (or square nut).
  - [M3 Washer](../../parts.yaml#m3-washer){qty: 4, cat: part_fastener}
- Motor:
  - [Remote motor base]{qty: 2, cat: man_3d} ([source](TC-RMT.FCStd)/[TCRMT-MOT-BASE.V1.stl](exported_models/TCRMT-MOT-BASE.V1.stl){previewpage})
  - [NEMA 17 Stepper Motor](../../parts.yaml#nema17-stepper-motor){qty: 1, cat: electronics}
  - [NEMA 17 5-8 Shaft Coupler](../../parts.yaml#nema17-5-8-shaft-coupler){qty: 1}
  - [M3 Hex Standoff 25mm](../../parts.yaml#m3-hex-standoff-25mm){qty: 2}
  - [OD8 L2 S1 THSL Leadscrew 136mm](../../parts.yaml#thsl-leadscrew-OD8-L2-S1-136mm){qty: 1}
  - [M3 x 10mm Screw](../../parts.yaml#m3-screw-10mm){qty: 3, cat: part_fastener}
  - [M3 x 16mm Screw](../../parts.yaml#m3-screw-16mm){qty: 1, cat: part_fastener}
  - [M3 Washer](../../parts.yaml#m3-washer){qty: 1, cat: part_fastener}
  - [500mm NEMA 17 Cable](../../parts.yaml#nema17-cable-500mm){qty: 1, cat: electronics}
- Linear guide:
  - [8x136mm Steel Rod](../../parts.yaml#steel-rod-8x136mm){qty: 2}
  - [LM8UU Linear Bearing](../../parts.yaml#bearing-linear-lm8uu){qty: 2}
  - Rod fasteners:
    - [M3 x 10mm Screw](../../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
    - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener} (or regular hex nut) (or square nut).
- Carriage:
  - [Remote actuator carriage]{qty: 1, cat: man_3d} ([source](TC-RMT.FCStd)/[TCRMT-CAR.V1.stl](exported_models/TCRMT-CAR.V1.stl){previewpage})
  - [THSL Nut D8xL2](../../parts.yaml#thsl-nut-ID8-L2-S1){qty: 1}
  - [M3 x 30mm Screw](../../parts.yaml#m3-screw-30mm){qty: 4, cat: part_fastener}
  - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
  - Endstop:
    - [MakerBot Endstop Remote Cover]{qty: 1, cat: man_3d} ([source](ENDSTOP-COVER.FCStd)/[ENDSTOP-REMOTE-V3-cover.stl](exported_models/ENDSTOP-REMOTE-V3-cover.stl){previewpage})
    - [Makerbot Endstop](../../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
    - [Makerbot Endstop Cable 500mm](../../parts.yaml#makerbot-endstop-cable-500mm){qty: 1, cat: electronics}
    - [M3 x 16mm Screw](../../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
    - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
  - Cable fixture:
    - [M3 x 30mm Screw](../../parts.yaml#m3-screw-30mm){qty: 2, cat: part_fastener}
    - [M3 Washer](../../parts.yaml#m3-washer){qty: 2, cat: part_fastener}
    - [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
    - Optionally, add a [Square Ferrule 1.5mm ID](../../parts.yaml#ferrule-ID1.5mm){qty: 1}.
    - Alternatively, wrap the cable around an M6 bolt, and secure it with washers and nut.
