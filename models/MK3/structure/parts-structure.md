# Structural Frame

Download a zip file containing all the [model files](structure-models.zip){zip, pattern:"structure/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [Miter Saw](../parts.yaml#miter-saw){qty: 1, cat: tool}
- [Metal Files](../parts.yaml#metal-files){qty: 1, cat: tool}
- [Machinist's Square](../parts.yaml#machinists-square){qty: 1, cat: tool}
- [Level Tool](../parts.yaml#level-tool){qty: 1, cat: tool}
- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}
- [M4 Tap](../parts.yaml#m4-tap){qty: 1, cat: tool}

Materials:

- [2020 V-Slot Aluminum 400mm](../parts.yaml#2020-v-slot-aluminum-400mm){qty: 8}
- [2020 V-Slot Aluminum 600mm](../parts.yaml#2020-v-slot-aluminum-600mm){qty: 4}
- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Structural parts

### Main frame

Parts:

- [M5 x 10 mm Button Head Screw](../parts.yaml#m5-screw-10mm-button-head){qty: 16, cat: part_fastener}

### Additions

Parts:

- Articulated legs:
  - [Articulated Leg V2]{qty: 4, cat: man_3d} ([source](ARTICULATED-FOOT.FCStd)/[ellipsoid-articulated-foot.stl](exported_models/ellipsoid-articulated-foot.stl){previewpage})
  - [M5 x 40 mm Screw](../parts.yaml#m5-screw-40mm){qty: 4, cat: part_fastener}
  - [M5 Hex Nut](../parts.yaml#m5-nut-hex){qty: 8, cat: part_fastener}
- Corner plates:
  - [2020 Corner Plate]{qty: 6, cat: man_3d} ([source](2020-CORNER-PLATE.FCStd)/[2020-CORNER-PLATE-M3_M4.V1.stl](exported_models/2020-CORNER-PLATE-M3_M4.V1.stl){previewpage})
  - [Printed 2020 Drop-in Nut]{qty: 26, cat: man_3d} ([2020-DROP-NUT.V1.stl](exported_models/2020-DROP-NUT.V1.stl){previewpage})
