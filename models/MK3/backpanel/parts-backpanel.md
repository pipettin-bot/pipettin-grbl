# Backpanel

Download a zip file containing all the [model files](backpanel-models.zip){zip, pattern:"backpanel/exported_models/*"}. The total amount of each part that should be made is in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Back-panel parts

Tools:

- [CNC Laser Cutter](../parts.yaml#cnc-laser-cutter){qty: 1, cat: tool}
- [3D Printer](../parts.yaml#3d-printer){qty: 1, cat: tool}

Materials:

- [PLA Filament 1.75mm](../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}
- [650 x 400 x 8 mm Acrylic Sheet](../parts.yaml#acrylic-sheet-650x400x8){qty: 1, cat: stock} for the back-panel.

## Cut the backpanel {pagestep}

Cut the backpanel using 8 mm acrylic stock.

A "DXF" file for cutting can be generated from the [FreeCAD model of the backpanel](BACKPANEL.FCStd).

Instructions for this are available in [our knowledgebase](https://docs.openlabautomata.xyz/Knowledgebase/CNC-Laser-Cutting/#on-your-pc-dxf-export).

## Mount the panel {pagestep}

Parts:

- [M4 Drop-In Nut 2020](../parts.yaml#m4-drop-in-nut-2020){qty: 6, cat: part_fastener}
- [M4 x 12mm Screw](../parts.yaml#m4-screw-12mm){qty: 6, cat: part_fastener}.
  - A 10 mm one may work, and up to 14 mm. Use washers as spacers for longer screws.
- [M4 Washer](../parts.yaml#m4-washer){qty: 6, cat: part_fastener}

## Cable bases {pagestep}

Parts, either of:

- Pairs of [cable base with lever]{qty: 3, cat: man_3d} ([source](cable_base.FCStd)/[cable_base-lever.stl](exported_models/cable_base-lever.stl){previewpage}).
  - [M3 Drop-In Nut 2020](../parts.yaml#m3-drop-in-nut-2020){qty: 6, cat: part_fastener}.
  - [M3 x 16mm Screw](../parts.yaml#m3-screw-16mm){qty: 9, cat: part_fastener}.
  - [M3 Hex Nut](../parts.yaml#m3-nut-hex){qty: 3, cat: part_fastener}
  - [M3 Washer](../parts.yaml#m3-washer){qty: 3, cat: part_fastener}
- [Cable base with insert]{qty: 0, cat: man_laser} ([source](cable_base.FCStd)/[cable_base-insert.step](exported_models/cable_base-insert.step)) (alternative).
