# This project's BOM was generated with GitBuilding

[Bill of Materials](index_BOM.csv).

To generate the bill of materials (BOM) run the following, and then visit <http://localhost:6178/index_BOM.html> to download the BOM in CSV format.

```bash
# Install GitBuilding.
python -m venv .venv
pip install gitbuilding

# Run gitBuilding.
gitbuilding build-html

# Get the BOM file.
cp _site/index_BOM.csv bom/

# Cleanup.
rm -rf _site
```

## What is GitBuilding

GitBuilding is an OpenSource project for documenting hardware projects with minimal
effort, so you can stop writing and GitBuilding. GitBuilding is a python program that
works on Windows, Linux, and MacOS. More information on the GitBuilding project, or how
to install GitBuilding please see the [GitBuilding website](http://gitbuilding.io)
