# Tools

Tools for the pipettin' bot.

Here is the complete [Bill of materials]{BOM} for the guides included below.

## Micropipettes

- [.](./pmulti-MK1/parts-pmulti.md){step}
- Adapter for Gilson-type micropipettes (BOM due, CAD model [AS-GIL-JUB.Fcstd](./gilson_adapter/AS-GIL-JUB.Fcstd)).
