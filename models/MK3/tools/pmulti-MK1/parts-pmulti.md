# pMulti micropipette

Download the zip files containing the 3D models:

- [Micropipette model files](tools-pmulti-models.zip){zip, pattern:"tools/pmulti-MK1/pipette/exported_models/*"}.
- [Tip ejector models](tools-pmulti-ejector.zip){zip, pattern:"tools/pmulti-MK1/fixed_ejector/exported_models/*"}.
- [Parking post models](tools-parking.zip){zip, pattern:"tools/parking/exported_models/*"}.
  - Alternative [tool-wings for 8 mm rods](../parking/exported_models/TP-JUB-WING-BASE.V3-8mm.stl) are available.

The total amount of each part that should be made is listed in the below and in the BOM.

Here is the complete [Bill of materials]{BOM} for this guide.

## Tools and Materials

Tools:

- [3D Printer](../../parts.yaml#3d-printer){qty: 1, cat: tool}
- [CNC Lathe](../../parts.yaml#cnc-lathe){qty: 1, cat: tool}
- [Drill]{qty: 1, cat: tool}
- [6 mm drill bit]{qty: 1, cat: tool}
- [M5 Tap](../../parts.yaml#m5-tap){qty: 1, cat: tool}
- [Silicone Grease](../../parts.yaml#grease-silicone){qty: 1, cat: tool}

Materials:

- [20 mm diameter Delrin stock]{qty: 1, cat: stock}
- [PLA Filament 1.75mm](../../parts.yaml#filament-pla-1.75){qty: 1, cat: stock}

## Parts

Parts for OLA's pOne micropipette.

### Tool-plate

- [M5 x 8 mm Button Head Screw](../../parts.yaml#m5-screw-8mm-button-head){qty: 3, cat: part_fastener}
- [M3 x 16mm Screw](../../parts.yaml#m3-screw-16mm){qty: 4, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}
- [pMulti tool-plate N]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[TOOLPLATE-WEDGE-PIP-N.stl](pipette/exported_models/TOOLPLATE-WEDGE-PIP-N.stl){previewpage})
- [THSL wedge-plate N]{qty: 1, cat: man_3d} ([source](../../toolchanger/head_plate/THSL-8x4-WEDGE-PLATE.FCStd)/[THSL-WEDGE-PLATE-BEVEL-M3-COMP.stl](pipette/exported_models/THSL-WEDGE-PLATE-BEVEL-M3-COMP.stl){previewpage})
- [M3 x 8mm Conical Head Screw](../../parts.yaml#m3-screw-8mm-conical-head){qty: 3, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 3, cat: part_fastener}
- Tool parking wings:
- [Parking wing base]{qty: 1, cat: man_3d} ([source](../parking/PIP-PARK-BASE.V2.1.FCStd)/[TP-PIP-BASE-V2.1.stl](../parking/exported_models/TP-PIP-BASE-V2.1.stl){previewpage})
- [Parking wing]{qty: 1, cat: man_3d} ([source](../parking/PIP-PARK-WINGS.V3.FCStd)/[TP-JUB-WING-BASE.V3.stl](../parking/exported_models/TP-JUB-WING-BASE.V3.stl){previewpage})
- [Parking wing (mirrored)]{qty: 1, cat: man_3d} ([source](../parking/PIP-PARK-WINGS.V3.FCStd)/[TP-JUB-WING-BASE.V3-MIRROR.stl](../parking/exported_models/TP-JUB-WING-BASE.V3-MIRROR.stl){previewpage})
- [Parking wing o-ring base]{qty: 4, cat: man_3d} ([source](../parking/PIP-PARK-WINGS.V3.FCStd)/[TP-JUB-WING-ORING.V3.stl](../parking/exported_models/TP-JUB-WING-ORING.V3.stl){previewpage})
- [6x2 Nitrile O-ring](../../parts.yaml#o-ring-nitrile-6x2){qty: 4}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 6, cat: part_fastener}
- [M3 x 16mm Screw](../../parts.yaml#m3-screw-16mm){qty: 4, cat: part_fastener}
- [M3 x 20mm Screw](../../parts.yaml#m3-screw-20mm){qty: 2, cat: part_fastener}

### Actuator

Motor:

- [NEMA 17 Short Body Stepper Motor](../../parts.yaml#nema17-short){qty: 1, cat: electronics}
- [NEMA 17 Cable 1000mm](../../parts.yaml#nema17-cable-1000mm){qty: 1, cat: electronics}
- [pMulti motor base]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[GIL-MOT-BAS.stl](pipette/exported_models/GIL-MOT-BAS.stl){previewpage})
- [THSL Leadscrew D8xL2 75mm](../../parts.yaml#thsl-leadscrew-OD8-L2-S1-75mm){qty: 1}
- [NEMA 17 5-8 Shaft Coupler](../../parts.yaml#nema17-5-8-shaft-coupler){qty: 1}
- [M3 x 10mm Screw](../../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
- [M3 Hex Standoff 10mm](../../parts.yaml#m3-hex-standoff-10mm){qty: 4}

Linear guide:

- [8x136mm Steel Rod](../../parts.yaml#steel-rod-8x136mm){qty: 2}
- [pMulti bottom base]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-BASE-LOWER.stl](pipette/exported_models/PIP-BASE-LOWER.stl){previewpage})
- [M3 x 10mm Screw](../../parts.yaml#m3-screw-10mm){qty: 4, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}

Endstop:

- [MakerBot Endstop](../../parts.yaml#makerbot-endstop){qty: 1, cat: electronics}
- [MakerBot Endstop Cable 1000mm](../../parts.yaml#makerbot-endstop-cable-1000mm){qty: 1, cat: electronics}
- [M3 x 10mm Screw](../../parts.yaml#m3-screw-10mm){qty: 2, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}
- [MakerBot endstop rod base]{qty: 2, cat: man_3d} ([source](pipette/PIP-ENDSTOP.FCStd)/[ENDSTOP-BASE-MAKERBOT.stl](pipette/exported_models/ENDSTOP-BASE-MAKERBOT.stl){previewpage})

Carriage:

- [pMulti actuator carriage]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-SUP-CR.stl](pipette/exported_models/PIP-SUP-CR.stl){previewpage})
- [LM8UU Linear Bearing](../../parts.yaml#bearing-linear-lm8uu){qty: 2}
- [THSL Anti-Backlash Nut D8xL2](../../parts.yaml#thsl-nut-D8-L2-S1-AB){qty: 1}
- [Compression spring 50 mm](../../parts.yaml#spring-comp-L50-CS0.5-OD.12-P6.25){qty: 2}
- [M3 x 12mm Screw](../../parts.yaml#m3-screw-12mm){qty: 2, cat: part_fastener}

### Cable management

On the tool side:

- [Cable base (8-wire)]{qty: 1, cat: man_3d} ([source](pipette/PIP-CABLES.FCStd)/[PIP-CABLE-BASE-8W.stl](pipette/exported_models/PIP-CABLE-BASE-8W.stl){previewpage})
  - Alternatively, for 4-wire cables, print [PIP-CABLE-BASE-4W.stl](pipette/exported_models/PIP-CABLE-BASE-4W.stl).
- [Zip Ties](../../parts.yaml#zip-tie){qty: 2}
- [Cable base rod fixture]{qty: 1, cat: man_3d} ([source](pipette/PIP-CABLES.FCStd)/[PIP-CABLE-BASE-ROD-FIXTURE.stl](pipette/exported_models/PIP-CABLE-BASE-ROD-FIXTURE.stl){previewpage})
- [M3 x 16mm Screw](../../parts.yaml#m3-screw-16mm){qty: 2, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 2, cat: part_fastener}

To guide the cables to the backpanel:

- [Reinforced Bowden Tube 1 meter](../../parts.yaml#1500m-reinforced-bowden-tube){qty: 1, cat: stock}
- One of the cable bases, as counted in the [backpanel BOM](../../backpanel/parts-backpanel.md).

### Pipette

Tip-holder:

- [Multi-fit Tip-holder]{qty: 1, cat: man_lathe} ([source](pipette/PIP-TIP-HOLDER-V02.FCStd)/[PZ0101-COMBINED-V02.step](pipette/exported_models/PZ0101-COMBINED-V02.step){previewpage})
- [6x2 Nitrile O-ring](../../parts.yaml#o-ring-nitrile-6x2){qty: 1}
- [pMulti tip-holder stop]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-CPL-SHEATH.stl](pipette/exported_models/PIP-CPL-SHEATH.stl){previewpage})
- [pMulti spring bottom shim]{qty: 2, cat: man_3d} ([source](pipette/PIP-TIP-HOLDER-V02.FCStd)/[SPRING-WASHER-v2.stl](pipette/exported_models/SPRING-WASHER-v2.stl){previewpage})
- [M3 x 25mm Screw](../../parts.yaml#m3-screw-25mm){qty: 4, cat: part_fastener}
- [M3 Hex Nut](../../parts.yaml#m3-nut-hex){qty: 4, cat: part_fastener}

Tip-holder and spring seats:

- Wide spring version:
  - [Compression Spring 17 mm (wide)](../../parts.yaml#spring-comp-L17-CS1-OD12-P4){qty: 1}
  - [pMulti spring top seat (wide)]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-SUP-LW.stl](pipette/exported_models/PIP-SUP-LW.stl){previewpage})
- Narrow spring version:
  - [Compression Spring 17 mm (narrow)](../../parts.yaml#spring-comp-L17-CS1-OD9-P4){qty: 1}
  - [pMulti spring top seat (narrow)]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-SUP-LW-ALT.stl](pipette/exported_models/PIP-SUP-LW-ALT.stl){previewpage})
  - [Spring centering shim]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-ALT-SPRING-SHIM.stl](pipette/exported_models/PIP-ALT-SPRING-SHIM.stl){previewpage})

Plunger:

- [M4 x 10mm Set screw](../../parts.yaml#m4-set-screw-10mm){qty: 2, cat: part_fastener}
- Dowel pin shaft and magnets version:
  - [50mm Dowel OD6 pin](../../parts.yaml#dowel-pin-OD6-L50){qty: 1}
  - [D6xL2 cylindrical magnet](../../parts.yaml#magnet-d6-l2){qty: 4}
  - [pMulti shaft holder (magnets)]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-SUP-STEM.stl](pipette/exported_models/PIP-SUP-STEM.stl){previewpage})
- Machined shaft and screw version:
  - [pMulti plunger (machined)]{qty: 1, cat: man_lathe} ([reference model](pipette/PLUNGER.FCStd))
  - [M4 x 16mm Conical Head Screw](../../parts.yaml#m4-screw-16mm-conical-head){qty: 1, cat: part_fastener} (16 mm thread length, 20 mm total length).
  - [pMulti shaft holder (screw)]{qty: 1, cat: man_3d} ([source](pipette/PIP-BASE.FCStd)/[PIP-SUP-STEM-ALT.stl](pipette/exported_models/PIP-SUP-STEM-ALT.stl){previewpage})

### Parking

Tool parking post

- [Tool post base]{qty: 1, cat: man_3d} ([source](../parking/PIP-POST-BASE.FCStd)/[TOOL-POST-5-8mm.stl](../parking/exported_models/TOOL-POST-5-8mm.stl){previewpage})
- [5x65mm Steel Rod Beveled](../../parts.yaml#steel-rod-5x65mm){qty: 2}
- [M4 Hex Nut](../../parts.yaml#m4-nut-hex){qty: 2, cat: part_fastener}
- [M4 x 25mm Screw](../../parts.yaml#m4-screw-25mm){qty: 2, cat: part_fastener}
- [M4 Washer](../../parts.yaml#m4-washer){qty: 2, cat: part_fastener}

Ejector post:

- [2020 V-Slot Aluminum 380mm](../../parts.yaml#2020-v-slot-aluminum-380mm){qty: 1}, M5 tapped on one end.
- [M5 x 20mm Screw](../../parts.yaml#m5-screw-20mm){qty: 1, cat: part_fastener}
- [2020 Angle Corner Connector](../../parts.yaml#2020-angle-corner-connector){qty: 2}
- [M4 Drop-In Nut 2020](../../parts.yaml#m4-drop-in-nut-2020){qty: 2, cat: part_fastener}
- [M4 x 10mm Screw](../../parts.yaml#m4-screw-10mm){qty: 2, cat: part_fastener}
- [M4 Washer](../../parts.yaml#m4-washer){qty: 2, cat: part_fastener}
- [Tip ejector fork]{qty: 1, cat: man_3d} ([source](fixed_ejector/PIP-TIP-EJECTOR.FCStd)/[PIP-TIP-EJECTOR.V1B.stl](fixed_ejector/exported_models/PIP-TIP-EJECTOR.V1B.stl){previewpage})
- [M5 x 20mm Screw](../../parts.yaml#m5-screw-20mm){qty: 1, cat: part_fastener}
- [M5 Washer](../../parts.yaml#m5-washer){qty: 2, cat: part_fastener}
- [M5 Hex Nut](../../parts.yaml#m5-nut-hex){qty: 1, cat: part_fastener}
