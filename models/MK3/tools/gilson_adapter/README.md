# Models for pipette tools

Models in this directory are for the printed parts of the Gilson p200, p20 and p1000 pipette adapter and actuator.

There are a few versions, one for each toolchanging mechanism.

## Adapter and actuator for "Gilson" micro-pipettes

CAD file organization:

- [GIL-TIP-PRB.Fcstd](./GIL-TIP-PRB.Fcstd): Base parts for the tip probes and assemblies.
- [GIL-BAS.Fcstd](./GIL-BAS.Fcstd): Base parts for the pipette adapters.
- Kinematic coupling mechanism (a Jubilee mod):
  - [AS-GIL-JUB.Fcstd](./AS-GIL-JUB.Fcstd): Part assembly parts for the pipette adapter and actuator.
- Latch-magnet mechanism (pipettin-zero robot):
  - [AS-TP-LCH.Fcstd](./AS-TP-LCH.Fcstd): Complete part assembly of the "latch" system for tool-parking, including the parked pieptte tool.
    - [TP-LCH-BAS.Fcstd](./TP-LCH-BAS.Fcstd): Tool-post base part.
    - [LCH-MINI.Fcstd](./LCH-MINI.Fcstd): Model for the "Mini-latch".
  - [AS-GIL-TC-LCH.Fcstd](./AS-GIL-TC-LCH.Fcstd): Full assembly of the pipette actuator, adding tool-changing parts.
    - [AS-GIL-TC-LCH-extras.Fcstd](./AS-GIL-TC-LCH-extras.Fcstd): Basic parts for the pipette actuator and accessory parts.
  - [AS-GIL.Fcstd](./AS-GIL.Fcstd): Basic assembly of the pipette actuator (non-changeable).

Contents:

[[_TOC_]]

----

> ![jubilee-compatible-adapter.png](./images/jubilee-compatible-adapter.png)
>
> Pipette actuator for the Pipettin-bot "one" robot. Adaptable to the Jubilee tool-changer.

> ![p20_tool_assembled.jpg](./images/p20_tool_assembled.jpg)
>
> Pipette actuator for the Pipettin-bot "zero" robot.

## Floating tip probe

This system uses 3D-printed parts and an opto-endstop to place tips in the fixed-depth way (constant pressure would be the other way, but was harder to achieve).

All models are here: [tools-pipette-gilson-tip_probe.FCStd](./tools-pipette-gilson-tip_probe.FCStd)

> ![IMG_7434.JPG](./images/IMG_7434.JPG)
>
> The tip sensor is "floating", allowing tips to be detected when placed and when ejected .

> ![p20_tip_probe.png](./images/p20_tip_probe.png)
>
> p20 (left) and p200 (right) pipette tip probes.

> ![](images/02-tip_probe.png)
>
> First prototype probe, for the p200 pipette.

## Toolpost for tool-changing

### Latch and magnet mechanism

Part assembly for the tool-parking system.

![toolpost_assembly.png](./images/toolpost_assembly.png)

### Jubilee-mod

Minimal FreeCAD rewrite of the toolpost (some stuff from [the original](https://github.com/machineagency/jubilee/tree/main/tools/jubilee_tools/tool_posts/configurable_tool_post) wasn't ported).

![jubilee-gil-post.png](./images/jubilee-gil-post.png)

# To-Do

- [ ] Add Pipette Jockey's Dragon Lab pipette adapter! https://youtu.be/BhQub5Xh_8o?t=1668
