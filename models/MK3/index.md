# Pipettin Bot MK3

## BOM

Here is the complete [Bill of materials]{BOM}.

## Sub-assemblies

Part lists for smaller chunks of the robot:

- [.](structure/parts-structure.md){step}
- [.](backpanel/parts-backpanel.md){step}
- [.](baseplate/parts-baseplate.md){step}
- [.](motion_system/parts_mk3.2.md){step}
- [.](toolchanger/parts-toolchanger.md){step}
- [.](tools/parts-tools.md){step}
- [.](electronics/parts-electronics.md){step}

<!--
Older assemblies, for reference:

- Motion system:
  - [MK3](motion_system/parts_mk3.md)
  - [MK3.1](motion_system/parts_mk3.1.md) (work in progress)
-->
