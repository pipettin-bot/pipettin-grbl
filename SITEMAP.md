# Repo map

Get your bearigs: learn how to navigate the repo here. Contents:

[[_TOC_]]

## Repository layout

Each subdirectory groups different kinds of source material. In each one, you'll find a `README.md` explaining its contents, and linking to other parts of the repo or wiki.

This repository uses "git submodules" to include and keep trak of the every other repository of the project. Those can be browsed here: https://gitlab.com/pipettin-bot

## [Docs and Guides](./GUIDES.md)

Links to guides and documentation are available at: [GUIDES.md](./GUIDES.md)

The main assembly documentation has moved to StepWiseDocs.com: <https://www.stepwisedocs.com/docs/projects>

If you are looking for development or usage documentation, find it in the documentation wiki [here](https://docs.openlabautomata.xyz/).

## CAD files: [`models`](./models)

Source model files for the machine's parts, modeled mostly with [FreeCAD](https://www.freecadweb.org/).

Most models are being migrated to a single folder, here: [all_models](./models/all_models)

## Software modules: [`code`](./code)

All software and firmware for the pipettin-bot project in one [code](./code) directory.

Most are included as git submodules.

# Legacy

Older versions of this repository are available in "archive" branches:

- [master-backup-v1-archive](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-v1-archive)
    - Legacy components: GRBL, pigpiod, protocol2gcode, pure JS protocol designer GUI, old directory layout for models.

## Default workspaces and objects: [`defaults`](./defaults) (deprecated)

JSON definition for the "default" GUI workspaces, platforms, protocols, etc.

Link to archive branch: https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-v1.6-old-docs-folder

## Development documentation: [`doc`](./doc) (deprecated)

Old documentation directory (currently in migration to the doc repo and/or other project subdirectories).

Content in this directory was referenced in the repo's READMEs when relevant.

Link to archive branch: https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-v1.6-old-docs-folder
