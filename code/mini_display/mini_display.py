#!/usr/bin/env python
# coding: utf-8

# Mini Display
# 
# Code to display system status on a small OLED display conneted to the Raspberry Pi's GPIO header using I2C.

# IP address

import netifaces
import subprocess

def get_ip_address():
    try:
        # Get the list of available network interfaces
        interfaces = netifaces.interfaces()
        # Iterate over the interfaces to find the wireless one
        for iface in interfaces:
            if iface.startswith('wlan'):
                # Get the addresses associated with the interface
                addresses = netifaces.ifaddresses(iface)
                # Get the IPv4 address if available
                if netifaces.AF_INET in addresses:
                    ip_info = addresses[netifaces.AF_INET][0]
                    ip_address = ip_info['addr']
                    # Get the name of the WiFi network (SSID)
                    ssid = subprocess.check_output(
                        ["iwgetid", "-r"], universal_newlines=True
                    ).strip()
                    if ssid:
                        return ip_address, ssid
                    else:
                        return "Not Connected", ":("
        # If no wireless interface is found or no IP address, raise an exception
        raise Exception("Not connected to a wireless network")
    except Exception:
        return "Not Connected", ":("


# Load image as bitmap

from PIL import Image

def convert_image_to_bitmap(image_path, output_path, width=24, height=24):
    # Open the image file
    with Image.open(image_path) as img:
        # Resize the image to 24x24 pixels
        img = img.resize((width, height), Image.Resampling.LANCZOS)
        # Convert the image to grayscale
        img = img.convert("1")  # Convert to 1-bit pixels, black and white
        # Save the image as a bitmap
        img.save(output_path)

# Resize the bitmap logo.
size = 36
convert_image_to_bitmap("bitmap_logo.png", "output_image.bmp", size, size)

# Rotating logo

from PIL import Image

# Load the bitmap image.
bitmap_path = "output_image.bmp"
bitmap_image = Image.open(bitmap_path)

def paste_rotated_image(image, bitmap_image, angle, WIDTH, HEIGHT, size):
    # Rotate the image.
    rotated_image = bitmap_image.rotate(-angle, resample=Image.Resampling.NEAREST, expand=False)
    
    # Position the bitmap in the center.
    # position = (oled.width // 2 - rotated_image.width // 2, oled.height // 2 - rotated_image.height // 2)

    # Make a copy of the image.
    new_image = image.copy()

    # Get drawing object to draw on image.
    new_image.paste(rotated_image, (WIDTH-size, HEIGHT-size))  # Position the bitmap in the corner.
    
    return new_image


# System usage bars

import psutil

def draw_usage_bars(draw):
    
    # Get CPU and RAM usage
    cpu_usage = psutil.cpu_percent()
    ram_usage = psutil.virtual_memory().percent
    
    # Bar dimensions
    bar_width = 32
    bar_height = 8
    bar_x = 32
    bar_y_cpu = HEIGHT - 2*(bar_height + 2)
    bar_y_ram = HEIGHT - 1*(bar_height + 2)
    
    # Draw CPU bar
    draw.rectangle((bar_x, bar_y_cpu, bar_x + bar_width, bar_y_cpu + bar_height), outline=255, fill=0)
    draw.rectangle((bar_x, bar_y_cpu, bar_x + int(bar_width * cpu_usage / 100), bar_y_cpu + bar_height), outline=255, fill=255)
    
    # Draw RAM bar
    draw.rectangle((bar_x, bar_y_ram, bar_x + bar_width, bar_y_ram + bar_height), outline=255, fill=0)
    draw.rectangle((bar_x, bar_y_ram, bar_x + int(bar_width * ram_usage / 100), bar_y_ram + bar_height), outline=255, fill=255)
    
    # Draw labels
    draw.text((0, bar_y_cpu - 2), f"CPU:", font=font, fill=255)
    draw.text((0, bar_y_ram - 2), f"RAM:", font=font, fill=255)


# Display setup

import digitalio
import board
# https://learn.adafruit.com/circuitpython-essentials/circuitpython-pins-and-modules
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont

# Change these to the right size for your display!
WIDTH = 128
HEIGHT = 64

# Use for I2C.
i2c = board.I2C()  # uses board.SCL and board.SDA
# i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller

# Define the Reset Pin
# TODO: Re-write this for compatibility with the Pi 5, which no longer supports the "RPi.GPIO" library.
# oled_reset = digitalio.DigitalInOut(board.D4)

# Define the OLED object.
# oled = adafruit_ssd1306.SSD1306_I2C(WIDTH, HEIGHT, i2c, addr=0x3C, reset=oled_reset)
oled = adafruit_ssd1306.SSD1306_I2C(WIDTH, HEIGHT, i2c, addr=0x3C)

# Clear display.
oled.fill(0)
oled.show()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
base_image = Image.new("1", (oled.width, oled.height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(base_image)

# Load default font.
font = ImageFont.load_default()

# Systemd service status

# List of services to check
services = [
    "klipper", "moonraker", "nginx", "ip-monitor",
    "nodegui", "piper", "jupyterhub", "mongod"
]
is_user_services = [
    False, False, False, False,
    False, False, False, False,
]

import subprocess
import os

os.environ["DBUS_SESSION_BUS_ADDRESS"] = "unix:path=/run/user/1000/bus"  # Replace with the address you found

def get_service_status(service_name, is_user_service=False):
    """
    Get the status of a systemd service.
    
    Parameters:
        service_name (str): The name of the systemd service.
        is_user_service (bool): True if the service is a user service, False if it's a system service.
    
    Returns:
        str: The status of the service ("active", "inactive", "failed", or "not found").
    """
    # Construct the systemctl command
    command = ["systemctl", "status", service_name]
    
    if is_user_service:
        command.insert(1, "--user")

    # print(f"Querying service: {' '.join(command)}")
    
    try:
        # Run the systemctl command
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        
        # Parse the output for the service status
        if result.returncode == 0:
            if "active (running)" in result.stdout:
                return "active"
            elif "inactive (dead)" in result.stdout:
                return "inactive"
            elif "failed" in result.stdout:
                return "failed"
        print(f"Query failed for service {service_name}: {result.stderr}")
        return "unknown"
    except FileNotFoundError:
        return "systemctl not found"


from PIL import ImageDraw

def draw_service_statuses(draw, service_names, is_user_service_flags, start_x=0, start_y=0, square_size=10, spacing=5):
    """
    Draw the status of systemd services as symbols on the display.
    
    Parameters:
        draw (ImageDraw.Draw): The ImageDraw object to draw on.
        service_names (list): A list of systemd service names.
        is_user_service_flags (list): A list of booleans indicating if each service is a user service.
        start_x (int): X-coordinate to start drawing.
        start_y (int): Y-coordinate to start drawing.
        square_size (int): The size of each square symbol.
        spacing (int): Spacing between each square.
    """
    for i, service_name in enumerate(service_names):
        status = get_service_status(service_name, is_user_service_flags[i])
        x = start_x + i * (square_size + spacing)
        y = start_y
        
        # Draw the square
        draw.rectangle([x, y, x + square_size, y + square_size], outline=255, fill=0)
        
        # Fill or mark based on status
        if status == "active":
            # Filled square for active
            draw.rectangle([x + 1, y + 1, x + square_size - 1, y + square_size - 1], outline=255, fill=255)
        elif status == "failed":
            # Square with a cross for failed
            draw.line([x + 1, y + 1, x + square_size - 1, y + square_size - 1], fill=255, width=1)
            draw.line([x + square_size - 1, y + 1, x + 1, y + square_size - 1], fill=255, width=1)
        elif status == "inactive":
            # Empty square for inactive (default drawn above)
            pass
        else:
            # Draw a "?" for unknown status
            font = ImageFont.load_default()
            text_x = x + (square_size // 2) - 3  # Center the "?" in the square
            text_y = y + (square_size // 2) - 6
            draw.text((text_x, text_y), "?", font=font, fill=255)
            print(f"Service {service_name} status not matched: {status}")

def render_service_status(draw, start_x=10, start_y=10, square_size=9, spacing=3):
    # Draw statuses starting from (10, 10)
    draw_service_statuses(draw, services, is_user_services, start_x=start_x, start_y=start_y, square_size=square_size, spacing=spacing)


# Render function

import time

def render(base_image):
    # Rotate in steps.
    angle = 0
    angle_step = 3
    period = 30.0
    frame_duration = period / (360/angle_step)

    # Register the start time.
    start_time = time.time()
    elapsed_time = 0
    next_usage_update = 0
    next_ip_update = 0

    # Get drawing object to draw on image.
    image = base_image.copy()
    draw = ImageDraw.Draw(image)

    # Service status.
    render_service_status(draw, start_x=1, start_y=25, square_size=8, spacing=3)

    # Initialize image.
    image_with_logo = paste_rotated_image(image, bitmap_image, angle, WIDTH, HEIGHT, size)

    # Faster updates for this.
    while elapsed_time < period:

        # Update elapsed time.
        elapsed_time = time.time() - start_time
        
        # Draw rotating logo.
        new_angle = angle_step * (elapsed_time // frame_duration)
        if new_angle != angle:
            update_angle = True
            # Update logo with new rotation.
            image_with_logo = paste_rotated_image(image, bitmap_image, angle, WIDTH, HEIGHT, size)
            # Update angle.
            angle = new_angle

        # Draw IP.
        if next_ip_update < elapsed_time:
            update_ip = True
            # Get IP address and SSID
            ip_address, ssid = get_ip_address()
            # Clear text area.
            draw.rectangle((0, 0, WIDTH, 24), outline=0, fill=0)
            # Draw IP Address on the first line
            draw.text((0, 0), f"IP: {ip_address}", font=font, fill=255)
            # Draw SSID on the second line
            draw.text((0, 12), f"WiFi: {ssid}", font=font, fill=255)
            # Set next update 5 seconds in the future.
            next_ip_update = elapsed_time + 5.0

        # Draw resource usage
        if next_usage_update < elapsed_time:
            update_usage = True
            # Update resource usage bars.
            draw_usage_bars(draw)
            # Set next update 1 second in the future.
            next_usage_update = elapsed_time + 1.0

        if update_usage or update_angle or update_ip:
            # Display image.
            oled.image(image_with_logo)
            oled.show()

        # Reset flags.
        update_ip = False
        update_angle = False
        update_usage = False

        # Sleep for just the right time.
        loop_elapsed_time = (time.time() - start_time) - elapsed_time
        sleep_time = max(frame_duration - loop_elapsed_time, 0.0)
        time.sleep(sleep_time)

# Clear display.
oled.fill(0)
oled.show()

# Loop forever

while True:
    render(base_image)
