# Draw system status on a small OLED

The following information is displayed on the screen:

- Current IP.
- Current WiFi SSID.
- Status of the [relevant systemd services](#monitored-services) as little squares.
- System resource usage: CPU and RAM.
- A rotating logo, to indicate activity.

The code uses the `adafruit_ssd1306` python library to work.

## Using the code

The [mini_display systemd units](../systemd.units/) launch the main [mini_display.py script](./mini_display.py).

The [2024-07-17-oled_display.ipynb](2024-07-17-oled_display.ipynb) notebook is where the script is developed, and should be converted to a script in order to add new functionality.

The command to do this is: `jupyter nbconvert 2024-07-17-oled_display.ipynb --to python`

## Monitored services

The status of some systemd services is displayed as squares on the display:

1. Filled: active.
2. Crossed: failing.
3. Inactive: empty.
4. "?": Unknown.

The monitored services are defined in a list in the code:

```python
services = [
    "klipper", "moonraker", "nginx", "ip-monitor",
    "nodegui", "piper", "jupyterhub", "gitbuilding"
]
```

## Hardware

Connect the 128x64 monochrome OLED to the Raspberry Pi's I2C pins, on the GPIO header.

Wiring: <https://learn.adafruit.com/monochrome-oled-breakouts/python-wiring#adafruit-0-dot-96-128x64-oled-display-stemma-qt-version-i2c-wiring-3069572>

## Installation

Follow our [setup guides](https://docs.openlabautomata.xyz/Mini-Docs/Software/additional-software-setup/#mini-display).
