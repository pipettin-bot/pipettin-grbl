// ArduinoJson - https://arduinojson.org
// Copyright © 2014-2023, Benoit BLANCHON
// MIT License
//
// This example shows how to deserialize a JSON document with ArduinoJson.
//
// https://arduinojson.org/v6/example/parser/
//
// Quick start examples: https://arduinojson.org/

#include <ArduinoJson.h>

void setup() {
  // Initialize serial port
  SerialUSB.begin(9600);
  while (!SerialUSB) continue;

  // https://arduinojson.org/v6/how-to/change-the-timeout/
  // Indeed, the stream waits for the data coming from the other peer, but if there is no data for an extended period, the stream returns a timeout error. 
  // The timeout is set to 1 second by default, but you can change this value in your program. Set timeout to 10 seconds:
  // SerialUSB.setTimeout(10000);

  // StaticJsonDocument<N> allocates memory on the stack, it can be
  // replaced by DynamicJsonDocument which allocates in the heap.
  //
  // DynamicJsonDocument doc(200);
}

void loop() {
  // Check if the other Arduino is transmitting
  if (SerialUSB.available()) {
    
    // Peek on the next byte and skip it if it is a newline or carriage return.
    int next_byte = SerialUSB.peek();
    // https://forum.arduino.cc/t/recognizing-new-line-character/619160/2
    // https://forum.arduino.cc/t/how-to-check-cr-in-the-char-from-serial-read-function/180110/4
    // A character is enclosed in single quotes ('\n').
    // https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html
    // Newline \n has code 10.
    if(next_byte == 10 | next_byte == 13){
      SerialUSB.print("Skipping newline character: ");
      SerialUSB.println(next_byte);
      SerialUSB.read();
    } else {
      // Continue processing

      // Allocate the JSON document
      // This one must be bigger than the sender's because it must store the strings
      StaticJsonDocument<300> doc;

      // The following document is expected:
      // {"timestamp":1234,"value":687}
      // See: https://arduinojson.org/v6/how-to/do-serial-communication-between-two-boards/

      // Read the JSON document from the "link" serial port.
      // Note: JSON reading terminates when a valid JSON is received, not considering line breaks.
      // Adding them to the message will result in incomplete/empty errors from deserializeJson.
      DeserializationError err = deserializeJson(doc, SerialUSB);

      if (err == DeserializationError::Ok) 
      {
        // Print the values
        // (we must use as<T>() to resolve the ambiguity)
        SerialUSB.print("timestamp = ");
        SerialUSB.println(doc["timestamp"].as<long>());
        SerialUSB.print("value = ");
        SerialUSB.println(doc["value"].as<int>());
      } 
      else 
      {
        // Print error to the "debug" serial port
        SerialUSB.print("Error: deserializeJson() returned ");
        SerialUSB.println(err.c_str());
    
        // Flush all bytes in the "link" serial port buffer
        while (SerialUSB.available() > 0)
          SerialUSB.read();
      }
    }
    }
}

// See also
// --------
//
// https://arduinojson.org/ contains the documentation for all the functions
// used above. It also includes an FAQ that will help you solve any
// deserialization problem.
//
// The book "Mastering ArduinoJson" contains a tutorial on deserialization.
// It begins with a simple example, like the one above, and then adds more
// features like deserializing directly from a file or an HTTP request.
// Learn more at https://arduinojson.org/book/
// Use the coupon code TWENTY for a 20% discount ❤❤❤❤❤
