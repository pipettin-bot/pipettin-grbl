#include <Arduino.h>

// Include files from this project.
#include "json_rpc.h"

// Example variables.
float variable1 = 10;
float variable2 = 20;

void setup() {

  // Initialize serial communications at 115200 bps.
  // Use Serial instead of Serial to send messages if on an Arduino zero,
  // otherwise no messages will be sent to the serial monitor.
  // https://forum.arduino.cc/t/i-cannot-get-the-serial-print-to-work-in-zero/571174/6
  Serial.begin(115200);
  //while (!Serial) continue;

}

void loop() {
  // Parse incoming messages and run other JSON-RPC logic.
  parse_incoming();

  // Place the rest of your code here.
  // ...
}

// Parse incoming messages and run other JSON-RPC logic.
void parse_incoming(){
  // Run the JSON-RPC Block if there is a live Serial connection.
  if(Serial){
    // Send alive signal.
    send_alive();

    // Echo incoming serial messages, for debugging.
    // echo_serial();

    /*
        Parse JSON messages.
        This function is defined in "json_rpc.cpp" and is meant to be edited by you.
        Here two variables are passed as an example, in case they are needed by the methods defined within.
    */
    parse_json_from_serial(variable1, variable2);
  }
}
