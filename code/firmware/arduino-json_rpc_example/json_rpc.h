#ifndef JSON_RPC_H
#define JSON_RPC_H

#include <Arduino.h>
#include <ArduinoJson.h>  // Parse JSON messsages from the serial interface.

void parse_json_from_serial(float variable1, float variable2);
void send_alive();
void echo_serial();

#endif // JSON_RPC_H
