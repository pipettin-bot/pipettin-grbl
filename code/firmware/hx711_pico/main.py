# type: ignore

import sys, select

# MIT License
#
# Copyright (c) 2022 Daniel Robertson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import _thread
import time
from machine import Pin, ADC
from micropython import const
from rp2 import PIO, StateMachine, asm_pio
import uos

def save_offsets(offset1, offset2, trigger_value, filename="offsets.txt"):
    with open(filename, "w") as f:
        f.write(f"{offset1},{offset2},{trigger_value}\n")
    print(f"// Saved offsets to persistent storage: {(offset1, offset2, trigger_value)}")

def load_offsets(filename="offsets.txt", default1=0, default2=0, default3=100000):
    try:
        with open(filename, "r") as f:
            line = f.readline().strip()
            offset1, offset2, trigger_value = map(int, line.split(","))
            print(f"// Restored offsets from persistent storage: {(offset1, offset2, trigger_value)}")
            return offset1, offset2, trigger_value
    except Exception as e:
        print(f"// Error loading offsets: {e}")
        print(f"// Returning defaults: {(default1, default2, default3)}")
        return default1, default2, default3  # Return default offsets if file not found or other error

class hx711:
    """Control class for the 24-bit HX711 ADC sensor
    Datasheets:
      - https://www.digikey.com/htmldatasheets/production/1836471/0/0/1/hx711.html
      - https://cdn.sparkfun.com/datasheets/Sensors/ForceFlex/hx711_english.pdf
    """

    class _util:

        @classmethod
        def get_sm_from_pio(cls, pio: PIO, sm_index: int) -> StateMachine:
            """Returns the StateMachine object from the given index

            Args:
                pio (PIO): RP2040 PIO instance
                sm_index (int):

            Returns:
                StateMachine:
            """
            return pio.state_machine(sm_index)

        @classmethod
        def get_sm_index(cls, pio_offset: int, sm_offset: int) -> int:
            """Returns the global state machine index from given args

            Args:
                pio_offset (int): 0 or 1
                sm_offset (int):

            Returns:
                int: index between 0 and 7
            """
            return (pio_offset >> 2) + sm_offset

        @classmethod
        def get_pio_from_sm_index(cls, sm_index: int) -> PIO:
            """Returns the correct PIO object from the global state machine index

            Args:
                sm_index (int):

            Returns:
                PIO:
            """
            return PIO(sm_index >> 2)

        @classmethod
        def sm_drain_tx_fifo(cls, sm: StateMachine) -> None:
            """Clears the StateMachine TX FIFO

            Args:
                sm (StateMachine):

            Performs:
            pull( ) noblock
            https://github.com/raspberrypi/pico-sdk/blob/master/src/rp2_common/hardware_pio/pio.c#L252
            This may not be thread safe
            """
            while sm.tx_fifo() != 0: sm.exec("pull() noblock")

        @classmethod
        def sm_get(cls, sm: StateMachine) -> int|None:
            """Returns a value from the StateMachine's RX FIFO (NON-BLOCKING)

            Args:
                sm (StateMachine):

            Returns:
                int|None: None is returned if RX FIFO is empty
            """
            return sm.get() if sm.rx_fifo() != 0 else None

        @classmethod
        def sm_get_blocking(cls, sm: StateMachine) -> int:
            """Returns a value from the StateMachine's RX FIFO (BLOCKING)

            Args:
                sm (StateMachine):

            Returns:
                int:
            """
            while sm.rx_fifo() == 0: pass
            return sm.get()

    class rate:
        """Sampling rate helper for the HX711 sensor.
        This is only configurable in hardware, and this firmware must be made to match.
        """
        rate_10: int = const(0)
        rate_80: int = const(1)

    class gain:
        """Gain selection helper for the HX711 sensor."""
        gain_128: int = const(25)  # Selects channel A.
        gain_32: int = const(26)  # Selects channel B.
        gain_64: int = const(27)  # Selects channel A.

    class power:
        pwr_up: int = const(0)
        pwr_down: int = const(1)

    class _pio_prog:
        def __init__(self) -> None:
            pass
        def init(self, hx) -> None:
            pass
        def program(self) -> None:
            pass

    class pio_noblock(_pio_prog):

        # see: https://github.com/endail/hx711-pico-c/blob/main/src/hx711_noblock.pio
        PUSH_BITS: int = const(24)
        FREQUENCY: int = const(10000000)

        def __init__(self) -> None:
            super().__init__()

        def init(self, hx: hx711) -> None:
            hx._sm = StateMachine(
                hx._sm_index,
                self.program,
                freq=self.FREQUENCY,
                in_base=hx.data_pin,
                out_base=hx.clock_pin,
                set_base=hx.clock_pin,
                jmp_pin=None,
                sideset_base=hx.clock_pin
            )

        # pylint: disable=E,W,C,R
        @asm_pio(
            out_init=(PIO.OUT_LOW),
            set_init=(PIO.OUT_LOW),
            sideset_init=(PIO.OUT_LOW),
            out_shiftdir=PIO.SHIFT_LEFT,
            autopush=True,
            autopull=False,
            push_thresh=PUSH_BITS,
            fifo_join=PIO.JOIN_NONE
        )
        def program():

            set(x, 0) # default gain of 0

            label("wrap_target")
            wrap_target()

            set(y, 23) # read bits, 0 based

            wait(0, pin, 0)

            label("bitloop")
            set(pins, 1)
            in_(pins, 1)

            jmp(y_dec, "bitloop").side(0).delay(2 - 1) # T4

            pull(noblock).side(1)

            out(x, 2)

            jmp(not_x, "wrap_target").side(0)

            mov(y, x)

            label("gainloop")
            set(pins, 1).delay(2 - 1) # T3
            jmp(y_dec, "gainloop").side(0).delay(2 - 1) # T4

            wrap()

    READ_BITS: int = const(24)
    MIN_VALUE: int = const(-0x800000)
    MAX_VALUE: int = const(0x7fffff)
    POWER_DOWN_TIMEOUT: int = const(60) # us
    SETTLING_TIMES: list[int] = [ # ms
        const(400),
        const(50)
    ]
    SAMPLES_RATES: list[int] = [
        const(10),
        const(80)
    ]

    def __init__(
        self,
        clk: Pin,
        dat: Pin,
        sm_index: int = 0,
        prog: _pio_prog = pio_noblock()
    ):
        """Create HX711 object

        Args:
            clk (Pin): GPIO pin connected to HX711's clock pin.
            dat (Pin): GPIO pin connected to HX711's data pin.
            sm_index (int, optional): Global state machine index to use. The RP2040 has two PIO instances, numbered 0 and 1. Defaults to 0.
            prog (_pio_prog, optional): PIO program. Defaults to built-in pio_noblock().
        """

        self._mut = _thread.allocate_lock()
        self._mut.acquire()

        self.clock_pin: Pin = clk
        self.data_pin: Pin = dat
        self.clock_pin.init(mode=Pin.OUT)
        self.data_pin.init(mode=Pin.IN)

        self._sm: StateMachine
        self._sm_index: int = sm_index
        self._prog: __class__._pio_prog = prog

        prog.init(self)

        self._mut.release()

    def __bool__(self) -> bool:
        return self._sm.active()

    def __repr__(self) -> str:
        return "[HX711 - CLK: {}, DAT: {}, SM_IDX: {}]".format(self.clock_pin, self.data_pin, self._sm_index)

    def __enter__(self):
        return self

    def __exit__(self, ex_type, ex_val, ex_tb) -> None:
        # handle abrupt exits from locked contexts
        if self._mut.locked(): self._mut.release()
        self.close()

    def close(self) -> None:
        """Stop communication with HX711. Does not alter power state.
        """
        self._mut.acquire()
        self._sm.active(0)
        __class__._util.get_pio_from_sm_index(self._sm_index).remove_program(self._prog.program)
        self._mut.release()

    def set_gain(self, gain: int) -> None:
        """Change HX711 gain

        Args:
            gain (int):
        """
        self._mut.acquire()
        __class__._util.sm_drain_tx_fifo(self._sm)
        self._sm.put(gain)
        self._sm.get()
        __class__._util.sm_get_blocking(self._sm)
        self._mut.release()

    @classmethod
    def get_twos_comp(cls, raw: int) -> int:
        """Returns the one's complement value from the raw HX711 value

        Args:
            raw (int): raw value from HX711

        Returns:
            int:
        """
        return -(raw & +cls.MIN_VALUE) + (raw & cls.MAX_VALUE)

    @classmethod
    def is_min_saturated(cls, val: int) -> bool:
        """Whether value is at its maximum

        Args:
            val (int):

        Returns:
            bool:
        """
        return val == cls.MIN_VALUE

    @classmethod
    def is_max_saturated(cls, val: int) -> bool:
        """Whether value is at its maximum

        Args:
            val (int):

        Returns:
            bool:
        """
        return val == cls.MAX_VALUE

    @classmethod
    def get_settling_time(cls, rate: int) -> int:
        """Returns the appropriate settling time for the given rate

        Args:
            rate (int):

        Returns:
            int: milliseconds
        """
        return cls.SETTLING_TIMES[rate]

    @classmethod
    def get_rate_sps(cls, rate: int) -> int:
        """Returns the numeric value of the given rate

        Args:
            rate (int):

        Returns:
            int:
        """
        return cls.SAMPLES_RATES[rate]

    def get_value(self) -> int:
        """Blocks until a value is returned

        Returns:
            int:
        """
        self._mut.acquire()
        rawVal = __class__._util.sm_get_blocking(self._sm)
        self._mut.release()
        return self.get_twos_comp(rawVal)

    def get_value_timeout(self, timeout: int = 1000000) -> int|None:
        """Attempts to obtain a value within the timeout

        Args:
            timeout (int, optional): timeout in microseconds. Defaults to 1000000.

        Returns:
            int|None: None is returned if no value is obtained within the timeout period
        """

        endTime: int = time.ticks_us() + timeout
        val: int|None = None

        self._mut.acquire()

        while(time.ticks_us() < endTime):
            val = self._try_get_value()
            if val != None: break

        self._mut.release()

        return self.get_twos_comp(val) if val else None

    def get_value_noblock(self) -> int|None:
        """Returns a value if one is available

        Returns:
            int|None: None is returned if no value is available
        """
        self._mut.acquire()
        val = self._try_get_value()
        self._mut.release()
        return self.get_twos_comp(val) if val else None

    def set_power(self, pwr: int) -> None:
        """Changes the power state of the HX711 and starts/stops the PIO program

        Args:
            pwr (int):
        """

        self._mut.acquire()

        if pwr == __class__.power.pwr_up:
            self.clock_pin.low()
            self._sm.restart()
            self._sm.active(1)
        elif pwr == __class__.power.pwr_down:
            self._sm.active(0)
            self.clock_pin.high()

        self._mut.release()

    @classmethod
    def wait_settle(cls, rate: int) -> None:
        """Waits for the appropriate amount of time for values to settle according to the given rate

        Args:
            rate (int):
        """
        time.sleep_ms(cls.get_settling_time(rate))

    @classmethod
    def wait_power_down(cls) -> None:
        """Waits for the appropriate amount of time for the HX711 to power down
        """
        time.sleep_us(cls.POWER_DOWN_TIMEOUT)

    def _try_get_value(self) -> int|None:
        """Attempts to obtain a value if one is available

        Returns:
            int|None: None is returned if no value is available
        """
        words = __class__.READ_BITS / 8
        return self._sm.get() if self._sm.rx_fifo() >= words else None


#### LOAD-CELL INIT ####

def init_loadcell(clk_pin: int, dat_pin: int, sm_index: int,
                  gain: hx711.gain=None,
                  rate:hx711.rate=None,
                  wait_n_read: int=10):

    # 1. Initialize the hx711 with pin 14 as clock pin, and pin 15 as data pin.
    hx = hx711(clk=Pin(clk_pin), dat=Pin(dat_pin), sm_index=sm_index)

    # 2. power up
    hx.set_power(hx711.power.pwr_up)

    # Default to maximum gain and channel A if "True".
    if gain is True:
        gain = hx711.gain.gain_128

    # 3. [OPTIONAL] set gain and save it to the hx711
    # chip by powering down then back up.
    if gain is not None:
        hx.set_gain(gain)
        hx.set_power(hx711.power.pwr_down)
        hx711.wait_power_down()
        hx.set_power(hx711.power.pwr_up)

    # 4. wait for readings to settle
    if rate is None:
        rate = hx711.rate.rate_10
    hx711.wait_settle(rate)

    # Read values with a timeout.
    # if val := hx.get_value_timeout(250000):
    #     # value was obtained within the timeout period
    #     # in this case, within 250 milliseconds
    #     print(val)

    # Or see if there's a value, but don't block if not
    # if val := hx.get_value_noblock():
    #     print(val)

    # 5. read values
    # wait (block) until a value is read
    millis = time.ticks_ms()
    led_flag=True
    for i in range(wait_n_read):
        # Blink while looping here.
        led_flag = not led_flag
        output.value(led_flag)  # High

        _ = hx.get_value()

        # Debugging output.
        # print(val, time.ticks_ms() - millis)

        millis = time.ticks_ms()

    return hx

# Interactive serial interface.
class TermRead:
    """
    We create  a select.poll() object called spoll, and register the standard input (sys.stdin) with the POLLIN
    flag, indicating that we want to wait for the standard input to be ready for reading.

    When we call spoll.poll(0), the 0 parameter specifies a milliseconds timeout value of 0,
    which means that the poll() method will return immediately with a list of file descriptors that
    have events to process, or an empty list if no events are ready to be processed.

    This allows us to check if there's input available on sys.stdin without waiting for any input to arrive.

    Source: https://github.com/orgs/micropython/discussions/11448
    """
    def __init__(self):
        self.spoll = select.poll()
        self.spoll.register(sys.stdin, select.POLLIN)
        self.txt = ''

    def read(self):
        txt = ''
        while self.spoll.poll(0):
            txt += sys.stdin.read(1)
        return txt

    def readline(self):
        # Fills the "txt" attribute with unread incoming data, and returns its contents
        # only when a "newline" is received. Note that "read" above will capture characters
        # and disregard "newline"s.
        while self.spoll.poll(0):
            data = sys.stdin.read(1)
            if data == "\n":
                # Return immediately if a newline character was received.
                result = self.txt
                # Clear the saved contents.
                self.txt = ''
                return result
            self.txt += data
        return ''


# Output pin.
output = Pin(25, Pin.OUT)  # Built-in / onboard LED.
# Turn on the LED.
output.value(1)  # High

# Initialize load cells.
hx = init_loadcell(clk_pin=14, dat_pin=15, sm_index=0, gain=hx711.gain.gain_64)
hx2 = init_loadcell(clk_pin=16, dat_pin=17, sm_index=1, gain=hx711.gain.gain_64)

# Turn off the LED.
output.value(0)  # Low

# Direct cell reading from ADC (single output).
adc = ADC(Pin(26))  # GPIO 26, 27 and 28 have ADCs we can use.

# Serial interface
term = TermRead()

# Main program.
def loop(term, adc, hx1, hx2):
    # Load-cell variables.
    offset = 0
    offset2 = 0
    val = 0
    val2 = 0
    trigger_value = 100000
    # Load saved offsets if they exist.
    offset, offset2, trigger_value = load_offsets(
        default1=offset,
        default2=offset2,
        default3=trigger_value)

    # Infinite loop.
    while True:
        txt = term.readline()
        # Or: txt = term.read()

        # print("// Message: " + txt if txt != '' else 'No Message...')
        if txt == "%":
            print("// Offset adjusted from", offset, "to", val)
            offset = val
            offset2 = val2
            save_offsets(offset, offset2, trigger_value)  # Save the new offsets
        elif txt.startswith("?"):
            try:
                trigger_value = int(txt[1:])
            except:
                print(f"// Invalid trigger value: {txt[1:]}")
            else:
                save_offsets(offset, offset2, trigger_value)  # Save the new offsets
        elif txt == "q":
            print("// Quitting...")
            break
        elif txt:
            print("// Invalid command: " + txt)

        # Read from the load-cell.
        val = hx1.get_value()

        # Read from the second load-cell.
        val2 = hx2.get_value()

        # Read value from the Pico's ADC (0-65535 across voltage range 0.0v - 3.3v).
        adc_val = adc.read_u16()
        # Convert to 12-bit (https://forums.raspberrypi.com/viewtopic.php?t=327209).
        adc_val = adc_val >> 4

        # Report.
        adj_sum = (val - offset) + (val2 - offset2)
        print(
            # Pico's ADC
            "ADC_PICO:", adc_val,
            # First load-cell.
            ",ADC1:", val,
            # Second load-cell.
            ",ADC2:", val2,
            # Offsets
            ",Offset1:", offset,
            ",Offset2:", offset2,
            ",Adjusted_ADC1:", (val - offset),
            ",Adjusted_ADC2:", (val2 - offset2),
            ",Adjusted_sum:", adj_sum,

            # ",Elapsed time:", time.ticks_ms() - millis,
            sep="")

        millis = time.ticks_ms()

        if abs(adj_sum) > trigger_value:
            output.value(1)  # High
        else:
            output.value(0)  # Low

# Loop forever.
try:
    loop(term, adc, hx, hx2)
except KeyboardInterrupt:
    print("// Done.")

# 6. stop communication with HX711
# hx.close()
# hx2.close()
