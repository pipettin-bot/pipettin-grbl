// WiFi configuration
// https://lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/#configuring-the-esp8266-web-server-in-wifi-station-sta-mode
#include <ESP8266WiFi.h>

// Normal web server (example from lastminuteengineers).
#include <ESP8266WebServer.h>
// Async web server, see library examples: https://github.com/me-no-dev/ESPAsyncWebServer
// #include <ESPAsyncTCP.h>
// #include <ESPAsyncWebSrv.h>
// Websocket connection
#include <WebSocketsServer.h>

// Your WiFi's SSID & Password.
const char* ssid = "";  // Enter SSID here
const char* password = "";  //Enter Password here

// Create server.
ESP8266WebServer server(80);
// AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);

// Open loop motor control example
#include <SimpleFOC.h>

// Endstop stuff: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
// const byte interruptPin = 14;
const byte interruptPin = 2; // Wemos D1 mini pin "D4" / "GPIO2"
volatile byte state = LOW;
int counter = 0;
float counter_timestamp = millis();
float elapsed = 0;
float report_interval = 2000;  // ms
//void toggle() {
//  state = !state;
//}
unsigned long currentTime = 0;       // the last time the output pin was toggled
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 4;     // the debounce time; increase if the output flickers
void IRAM_ATTR toggle() {
  // Interrupts in ESP32: https://lastminuteengineers.com/handling-esp32-gpio-interrupts-tutorial/
  // Debouncing: https://docs.arduino.cc/built-in-examples/digital/Debounce
  currentTime = millis();
  if (currentTime - lastDebounceTime >= debounceDelay) {
    lastDebounceTime = currentTime;
    state = HIGH;
    counter++;
  }
}

// BLDC motor & driver instance
BLDCMotor motor = BLDCMotor(4);  // Pole pairs HDD
//BLDCMotor motor = BLDCMotor(7);  // Pole pairs Drone

// Reference:
// BLDCDriver3PWM driver = BLDCDriver3PWM(pwmA, pwmB, pwmC, Enable(optional));
// Boards:
// BLDCDriver3PWM driver = BLDCDriver3PWM(23, 22, 1, 3);  // ESP32 WROOM (old)
// BLDCDriver3PWM driver = BLDCDriver3PWM(17, 16, 4, 0);  // ESP32 WROOM (new)
// BLDCDriver3PWM driver = BLDCDriver3PWM(11, 10, 9, 8);  // Arduino UNO
// Lolin Wemos D1 Mini: D7(GPIO13), D6(GPIO12), D5(GPIO14), D0/GPIO16
BLDCDriver3PWM driver = BLDCDriver3PWM(13, 12, 14, 16);

// Stepper motor & driver instance
//StepperMotor motor = StepperMotor(50);
//StepperDriver4PWM driver = StepperDriver4PWM(9, 5, 10, 6,  8);

//target variable
float angular_velocity = 0.0;
float current_rpm = 0.0;
float ramp_increment = 0.2;
float ramp_increment_interval = 1e4;  // ms
float direction = 1.0;
float rpm_limit = 60.0;  // 2900.0;  // At 2940-2960 rpm and 3V the motor will stall.
float angular_velocity_limit = (rpm_limit / 60) * (2 * 3.14159265359);
// float angular_velocity_limit = 2.0*3.14159265359;

// timestamp for changing direction
float timestamp_us = _micros();

// timestamp for serial messages
float timestamp_ms = millis();

// instantiate the commander
Commander command = Commander(Serial);
void doTarget(char* cmd) {
  command.scalar(&rpm_limit, cmd);
}
void doLimit(char* cmd) {
  command.scalar(&motor.voltage_limit, cmd);
}

void setup() {

  // Endstop stuff: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), toggle, FALLING);
  // attachInterrupt(digitalPinToInterrupt(interruptPin), toggle, RISING);

  // Set built-in LED pin mode: https://circuits4you.com/2018/02/02/esp32-led-blink-example/
  // pinMode(LED_BUILTIN, OUTPUT);

  // driver config
  // power supply voltage [V]
  driver.voltage_power_supply = 12;
  // limit the maximal dc voltage the driver can set
  // as a protection measure for the low-resistance motors
  // this value is fixed on startup
  driver.voltage_limit = 10;
  driver.init();
  // link the motor and the driver
  motor.linkDriver(&driver);

  // limiting motor movements
  // limit the voltage to be set to the motor
  // start very low for high resistance motors
  // current = voltage / resistance, so try to be well under 1Amp

  // Openloop velocity control: https://docs.simplefoc.com/velocity_openloop

  // Set the open loop control type: https://docs.simplefoc.com/open_loop_motion_control
  // MotionControlType::velocity_openloop    - velocity open-loop control
  // MotionControlType::angle_openloop       - position open-loop control
  motor.controller = MotionControlType::velocity_openloop;

  // limiting voltage - [V] HDD BLDC
  // motor.voltage_limit = 2.25;  // Not too hot.
  // motor.voltage_limit = 3.0;   // Quite hot.
  // motor.voltage_limit = 3.5;   // TOO HOT.
  motor.voltage_limit = 5.0;  // CANT TOUCH THIS.

  // limiting voltage - [V] A2212/10T 1400kV 0.065 Ohm (from datasheet) --> 0.19V
  // motor.voltage_limit = 0.6;   // Ok

  // limiting current - if phase resistance provided
  motor.current_limit = 2.5;  // Amps - [A] HDD BLDC

  // The velocity open-loop control will (if not provided phase resistance),
  // set the voltage to the motor equal to the motor.voltage_limit

  // Set the torque control type: https://docs.simplefoc.com/voltage_torque_mode
  // motor phase resistance [Ohms]
  // motor.phase_resistance = 1.6; // Esta es la resistencia "phase to phase" (no es lo mismo que la "per phase"). ¿Cual tengo que poner?
  motor.phase_resistance = 0.8;  // Esta es la resistencia "phase to star point".
  // motor.torque_controller = TorqueControlType::voltage;
  // set motion control loop to be used
  // motor.controller = MotionControlType::torque;

  // Current limiting with Back-EMF compensation.
  // motor KV rating [rpm/V]
  motor.KV_rating = 1000;  // rpm/volt - HDD BLDC motor. Not measured, just guessed.
  // motor.KV_rating = 1400; // rpm/volt - A2212 T10 motor.

  // Datasheet: DRV8313 2.5-A Triple 1/2-H Bridge Driver
  // https://www.ti.com/lit/ds/symlink/drv8313.pdf
  // If the die temperature exceeds safe limits, all FETs in the H-bridge will be disabled and the nFAULT pin will be
  // driven low. Once the die temperature has fallen to a safe level operation will automatically resume. The nFAULT
  // pin will be released after operation has resumed.

  // init motor hardware
  motor.init();

  // add target command R and L.
  command.add('T', doTarget, "target velocity");
  command.add('L', doLimit, "voltage limit");

  Serial.begin(115200);
  Serial.println("Motor ready!");
  Serial.println("Set target velocity [rad/s]");
  _delay(1000);

  // Wifi setup.
  https://lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/#configuring-the-esp8266-web-server-in-wifi-station-sta-mode
  Serial.println("Connecting to ");
  Serial.println(ssid);
  
  //connect to your local wi-fi network
  WiFi.begin(ssid, password);
  
  // Check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());

  // Start websocket
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  
  // Register handlers.
  server.on("/", handle_OnConnect);
  // server.on("/updateRPM", HTTP_GET, handle_UpdateRPM);
  server.onNotFound(handle_NotFound);

  // Async wifi server.
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  // if (WiFi.waitForConnectResult() != WL_CONNECTED) {
  //     Serial.printf("WiFi Failed!\n");
  //     return;
  // }
  // 
  // Serial.print("IP Address: ");
  // Serial.println(WiFi.localIP());
  //
  // // Register async handlers.
  // server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
  //     request->send(200, "text/html", SendHTML());
  // });
  // server.onNotFound(notFound);
  // 
  // server.on("/updateRPM", HTTP_GET, [](AsyncWebServerRequest *request){
  //     if (request->args() > 0) {
  //         for (uint8_t i = 0; i < request->args(); i++) {
  //             if (request->argName(i) == "rpmLimit") {
  //                 rpm_limit = request->arg(i).toFloat();
  //             }
  //         }
  //     }
  //     request->send(200, "text/html", SendHTML());
  // });
  
  // Start the server.
  server.begin();
  Serial.println("HTTP server started");
}


// open loop velocity movement
void loop() {
  
  // Do WiFi server stuff.
  server.handleClient();
  // The async web server does not to run a function here.
  // ...
  // Do websocket server stuff.
  webSocket.loop();

  // The "rpm_limit" variable holds the final target RPM.
  // Its value is set by the "T" serial command, and also
  // by events from the websocket connection.

  // Compute current RPM.
  // The "angular_velocity" is the speed value passed to SimpleFOC.
  // Acceleration is implemented by increasing intermediate 
  // target RPM values up to the "rpm_limit" value.
  // It is initially zero and slowly increased/decreased until it reaches
  // the target velocity defined by "angular_velocity_limit" (which is in
  // turn derived from "rpm_limit").
  current_rpm = angular_velocity / (2 * 3.14159265359) * 60;

  // Print current target RPM.
  elapsed = millis() - timestamp_ms;
  if (elapsed > report_interval) {
    timestamp_ms = millis();
    Serial.print("Current_RPM:");
    Serial.print(current_rpm);
    Serial.print("Current RPM limit:");
    Serial.print(rpm_limit);
  }

  // Estimate RPM from the opto-endstop trigger count.
  elapsed = millis() - counter_timestamp;
  if (elapsed > report_interval) {
    Serial.print(",Estimated_RPM:");
    Serial.println(counter / (elapsed / (1000 * 60)));  // Convert ms to minutes and calculate RPM.
    counter_timestamp = millis();
    counter = 0;
  }

  // Auto-homing at low RPM.
  // If the interrupt has raised the state flag (i.e. is HIGH), 
  // and the current RPM is slow enough to stop, then stop.
  // This will be at the home position.
  if (state) {
    // Serial.println("Endstop triggered!");
    state = LOW;  // Reset state.
    if (current_rpm <= 11) {
      rpm_limit = 0.0;  // HALT IMMEDIATELY
    }
  }

  // User communication.
  // This can update the value of "rpm_limit".
  command.run();

  // Adjust voltage (probably not doing anything, since it is set in the setup)
  // if(rpm_limit >= 2500 & motor.voltage_limit < 5.0){
  //   Serial.println("High voltage!");
  //   motor.voltage_limit = 5.0;
  // }
  // if (rpm_limit < 2500 & motor.voltage_limit > 2.0) {
  //   Serial.println("Low voltage!");
  //   motor.voltage_limit = 2.0;
  // }

  // Stop immediately if requested.
  // This can be induced by the homing procedure (see above).
  if (rpm_limit <= 0.0) {

    // Set target speed to zero.
    angular_velocity = 0.0;

    // Disable motor if requested with negative RPM.
    // See: https://community.simplefoc.com/t/motor-disable/2479/2
    if (motor.enabled == 1 & rpm_limit < 0.0) {
      motor.disable();
      Serial.println("Motor disabled.");
    }

  } else {
    // Enable motor: https://community.simplefoc.com/t/motor-disable/2479/2
    if (motor.enabled == 0 & rpm_limit >= 0.0) {
      motor.enable();
      Serial.println("Motor enabled.");
    }
  }


  // Compute new velocity limit
  angular_velocity_limit = (rpm_limit / 60) * (2 * 3.14159265359);


  // Rudimentary acceleration control.
  // Applies only if the target angular velocity is non-zero.
  // We don't want the motor to "wiggle" around the homing position,
  // we want it to stay put!
  if(rpm_limit != 0.0){
    // Limit velocity by inverting acceleration
    // if( abs(angular_velocity) > angular_velocity_limit ){
    //   ramp_increment = -ramp_increment;
    // }
    if (angular_velocity > angular_velocity_limit) {
      // Speed-up.
      ramp_increment = -abs(ramp_increment);
    } else {
      // Slow-down.
      ramp_increment = abs(ramp_increment);
    }

    // Motor Aceleration.
    // Increase/decrease velocity a bit, every now and then.
    if (_micros() - timestamp_us > ramp_increment_interval) {
      timestamp_us = _micros();
      angular_velocity += ramp_increment;
    }
  }

  // Apply the new speed in SimpleFOC.
  // Using "motor.voltage_limit" and "motor.velocity_limit".
  motor.move(angular_velocity);
}

// Web socket handlers.
void handleWebSocketMessage(uint8_t num, String message) {
    Serial.printf("Message from client %u: %s\n", num, message.c_str());

    if (message.startsWith("rpmLimit:")) {
        rpm_limit = message.substring(9).toFloat();
        // You can perform additional actions based on the received message.
    }
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_TEXT:
            handleWebSocketMessage(num, String((char*)payload));
            break;
        default:
            break;
    }
}

// Not found handler for async server.
// void notFound(AsyncWebServerRequest *request) {
//     request->send(404, "text/plain", "Not found");
//}

// WiFi server handlers (normal version, not async).
void handle_OnConnect() {
  // LED1status = LOW;
  // LED2status = LOW;
  // Serial.println("GPIO7 Status: OFF | GPIO6 Status: OFF");
  // server.send(200, "text/html", SendHTML(LED1status,LED2status)); 
  server.send(200, "text/html", SendHTML()); 
}
// void handle_UpdateRPM() {
//     if (server.args() > 0) {
//         for (uint8_t i = 0; i < server.args(); i++) {
//             if (server.argName(i) == "rpmLimit") {
//               // Update target RPM value.
//                 rpm_limit = server.arg(i).toFloat();
//             }
//         }
//     }
//     // Send HTML.
//     server.send(200, "text/html", SendHTML());
// }
// 
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}


// Unused examples:
// void handle_led1on() {
//   LED1status = HIGH;
//   Serial.println("GPIO7 Status: ON");
//   server.send(200, "text/html", SendHTML(true,LED2status)); 
// }
// 
// void handle_led1off() {
//   LED1status = LOW;
//   Serial.println("GPIO7 Status: OFF");
//   server.send(200, "text/html", SendHTML(false,LED2status)); 
// }
// 
// void handle_led2on() {
//   LED2status = HIGH;
//   Serial.println("GPIO6 Status: ON");
//   server.send(200, "text/html", SendHTML(LED1status,true)); 
// }
// 
// void handle_led2off() {
//   LED2status = LOW;
//   Serial.println("GPIO6 Status: OFF");
//   server.send(200, "text/html", SendHTML(LED1status,false)); 
// }


// Web page contents.
String SendHTML(){
  // The first text you should always send is the <!DOCTYPE> declaration, which indicates that we’re sending HTML code.
  String ptr = "<!DOCTYPE html> <html>\n";
  
  // HTML head.
  // The <meta> viewport element makes the web page responsive, ensuring that it looks good on all devices. The title tag determines the page’s title.
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  
  // Page title.
  ptr +="<title>Motor Control</title>\n";
  
  // Styling the Web Page with some CSS.
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +=".button {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr +=".button-on {background-color: #1abc9c;}\n";
  ptr +=".button-on:active {background-color: #16a085;}\n";
  ptr +=".button-off {background-color: #34495e;}\n";
  ptr +=".button-off:active {background-color: #2c3e50;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  
  // JavaScript Function for Slider Interaction:
  // Add the following JavaScript code in the <head> section of your HTML page,
  // to update the RPM value displayed in the webpage, as the slider is moved:
  ptr += "<script>\n";
  ptr += "function updateRPM(value) {\n";
  ptr += "  document.getElementById('rpmValue').innerHTML = value;\n";
  ptr += "}\n";
  ptr += "</script>\n";
  
  // Button to send an RPM update to the server.
  // ptr += "<script>\n";
  // ptr += "function updateRPMOnServer() {\n";
  // ptr += "  var rpmValue = document.getElementById('rpmSlider').value;\n";
  // ptr += "  var url = '/updateRPM?rpmLimit=' + rpmValue;\n";
  // ptr += "  window.location.href = url;\n";
  // ptr += "}\n";
  // ptr += "</script>\n";
  
  // Websocket function
  ptr += "<script>\n";
  // Handle events.
  ptr += "var socket = new WebSocket('ws://' + window.location.hostname + ':81/');\n";
  ptr += "socket.onmessage = function (event) {\n";
  ptr += "  console.log('Message from server: ', event.data);\n";
  ptr += "  // You can perform actions based on the received message.\n";
  ptr += "}\n";
  // Update RPM.
  ptr += "function updateRPM(value) {\n";
  ptr += "  document.getElementById('rpmValue').innerHTML = value;\n";
  ptr += "  socket.send('rpmLimit:' + value);\n";
  ptr += "}\n";
  ptr += "</script>\n";
  
  // End of the HTML head.
  ptr +="</head>\n";
  
  // HTML body.
  ptr +="<body>\n";
  // Setting the Web Page Heading
  ptr +="<h1>ESP8266 Web Server</h1>\n";
  ptr +="<h3>Using Station(STA) Mode</h3>\n";

  // Displaying the Slider for RPM Control.
  ptr += "<input type=\"range\" id=\"rpmSlider\" min=\"-1\" max=\"7200\" value=\"";
  ptr += int(rpm_limit);
  ptr += "\" oninput=\"updateRPM(this.value)\">";
  // ptr += "<input type=\"range\" id=\"rpmSlider\" min=\"-1\" max=\"1000\" value=\"50\" oninput=\"updateRPM(this.value)\">";
  
  // Display current RPM.
  ptr += "<p>Current RPM: <span id=\"rpmValue\">";
  ptr += int(rpm_limit);
  ptr += "</span></p>\n";
  // ptr += "<p>Current RPM: <span id=\"rpmValue\">50</span></p>\n";
  
  // Add RPM update button.
  ptr += "<button onclick=\"updateRPMOnServer()\">Update RPM</button>\n";

  // Displaying the Buttons and Corresponding State.
  // if(led1stat){
  //   ptr +="<p>LED1 Status: ON</p><a class=\"button button-off\" href=\"/led1off\">OFF</a>\n";
  // } else {
  //   ptr +="<p>LED1 Status: OFF</p><a class=\"button button-on\" href=\"/led1on\">ON</a>\n";
  // }
  // 
  // if(led2stat){
  //   ptr +="<p>LED2 Status: ON</p><a class=\"button button-off\" href=\"/led2off\">OFF</a>\n";
  // } else { 
  //   ptr +="<p>LED2 Status: OFF</p><a class=\"button button-on\" href=\"/led2on\">ON</a>\n";
  // }
  
  // End of HTLM body.
  ptr +="</body>\n";
  
  // End of document.
  ptr +="</html>\n";
  
  return ptr;
}
