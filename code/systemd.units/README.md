# Systemd units for the Raspberry Pi

"Systemd units" handle the startup of the GUI's Node.js server, and of the Python driver module, and other services.

More information at: <https://docs.openlabautomata.xyz/Software-Setup/#systemd-units>

## Piper

Unit files:

- Main [piper](piper.service) unit.
- Updater [piper_update](piper_update.service) unit, this one runs once on boot before the main one, and by Moonraker on updates.

## OLED Display

Unit files:

- Main [mini_display](mini_display.service) unit.
- Updater [mini_display_prepare](mini_display_prepare.service) unit. This one runs once on boot before the main one, updating the script that controls the OLED.

## Pipettin Writer

Unit files:

- Main [nodegui](nodegui.service) unit.

For development, this file uses `nodemon` instead of `node`.

This requires `nodemon` to be installed. A compatible (legacy) `nodemon` version was installed with the following command:

```bash
# Details at https://github.com/remy/nodemon/issues/1948#issuecomment-953665876
sudo npm install -g nodemon@2.0.12
```

> Note: edit the unit file to use `node` instead of `nodemon` for deployments.

## Host IP updater

Unit files:

- Main [ip-monitor](host_updater/ip-monitor.service) user unit.

This unit should be enabled when the HOST address is likely to change due to, for example, a changing LAN IP.

To enable it, see [the host_updater's README](host_updater/README.md) file.

## Jupyter Lab

Unit file: [jupyter.service](./jupyter.service)

To be placed at `/etc/systemd/system/jupyter.service`.

```bash
sudo cp jupyter.service /etc/systemd/system/jupyter.service
sudo systemctl daemon-reload
sudo systemctl start jupyter.service
sudo systemctl status jupyter.service
sudo systemctl enable jupyter.service
```

# User units

Pipettin's units can be configured to run under the regular user, without root privileges.

> This approach has been replaced by setting the User and Group properties in the units to `pi`, and setting polkit rules to allow users to manage them.

Configure systemd [lingering](https://wiki.archlinux.org/title/systemd/User#Automatic_start-up_of_systemd_user_instances), at least once as the `pi` user:

```bash
sudo loginctl enable-linger pi
```

User unit definitions reside at `~/.config/systemd/user/` in the RPi's file system.

Create the directory, link the units, and reload the systemd user daemon:

```bash
# Check that the units are detected and are not in a "bad" state.
systemctl list-unit-files --user

# Reload the user's units
systemctl --user daemon-reload
```

# Obsolete units

The information below is kept here for reference, but **is no longer in use**.

## protocol2gcode

Located at: `systemd.units/commander.service`

Learn more about it at: [`protocol2gcode/README.md`](../protocol2gcode/README.md)

Start and enable the unit:

```bash
systemctl --user start commander.service
systemctl --user status commander.service
systemctl --user enable commander.service
```

## GitBuilding docs

Unit file: [gitbuilding.service](./gitbuilding.service)

Requirements:

- docs repo at `/home/pi/pipettin-grbl-docs/`
- venv with gitbuilding installed at `/home/pi/pipettin-grbl-docs/bot_venv`
- Learn more at: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

```bash
sudo cp gitbuilding.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start gitbuilding.service
sudo systemctl status gitbuilding.service
sudo systemctl enable gitbuilding.service
```

## MongoDB unit

Copy and enable the database's systemd unit: [mongodb.service](./mongo/mongodb.service)

```bash
sudo cp ~/pipettin-grbl/systemd.units/mongodb.service /etc/systemd/system/mongodb.service
sudo systemctl daemon-reload
sudo systemctl enable mongodb.service
sudo systemctl start mongodb.service
sudo systemctl status mongodb.service
```

An example configuration file for mongodb is available here: [mongod.conf](./mongo/mongod.conf)


