import psutil
import sys
import time
import os
import json
import socket
import subprocess
from pathlib import Path

# Path to the config.json file
CONFIG_FILE_PATH = "/home/pi/pipettin-bot/code/pipettin-gui/config.json"

# Systemd service name for the frontend
FRONTEND_SERVICE = "nodegui.service"

def get_lan_ips():
    """
    Get all LAN IPs of the machine from all network interfaces.
    """
    try:
        lan_ips = []
        for iface, addrs in psutil.net_if_addrs().items():
            for addr in addrs:
                # Excludes localhost (127.x.x.x).
                # if addr.address.startswith("127."):
                #     continue
                
                # Filters for IPv4 addresses (AF_INET).
                if addr.family.name == "AF_INET":
                    lan_ips.append(addr.address)
        return lan_ips
    except Exception as e:
        print(f"Error getting LAN IPs: {e}", file=sys.stderr)
        return []

def get_config_host():
    """Read the HOST IP from the config.json file."""
    try:
        with open(CONFIG_FILE_PATH, "r") as file:
            config = json.load(file)
        return config.get("HOST")
    except (FileNotFoundError, json.JSONDecodeError) as e:
        print(f"Error reading {CONFIG_FILE_PATH}: {e}")
        return None

def is_external(ip):
    """Sort LAN IPs to prioritize external IPs"""
    # Check for common private IP ranges
    private_ranges = [
        "10.",  # Class A private
        "172.16.", "172.17.", "172.18.", "172.19.", "172.20.", "172.21.", "172.22.",
        "172.23.", "172.24.", "172.25.", "172.26.", "172.27.", "172.28.", "172.29.",
        "172.30.", "172.31.",  # Class B private
        "192.168."  # Class C private
    ]
    return not any(ip.startswith(pr) for pr in private_ranges)

def is_internal(ip):
    """Sort LAN IPs to prioritize external IPs"""
    return ip.startswith("127")

def write_config_ip(new_host_ip):
    print(f"Writing IP {new_host_ip} to config file: {CONFIG_FILE_PATH}")
    # Load the existing configuration
    with open(CONFIG_FILE_PATH, "r") as file:
        config = json.load(file)
    # Update the HOST field
    config["HOST"] = new_host_ip
    # Write back the updated configuration
    with open(CONFIG_FILE_PATH, "w") as file:
        json.dump(config, file, indent=4)

def update_config_host_ip(lan_ips, default="127.0.0.1"):
    """
    Update the HOST IP in config.json, prioritizing external IPs.
    If no external IPs are available, fall back to the default '127.0.0.1'.
    """
    print(f"Updating IP in config from IPs {lan_ips} or defaulting to: {default}")
    try:
        # Sort LAN IPs: external IPs first, private IPs second
        sorted_ips = sorted(lan_ips, key=is_internal)

        # Select the first sorted IP or default to localhost
        new_host_ip = sorted_ips[0] if sorted_ips else default

        # Write the IP.
        write_config_ip(new_host_ip)

        print(f"Updated HOST in config.json to: {new_host_ip}")
        return new_host_ip
    except Exception as e:
        print(f"Error updating config.json: {e}", file=sys.stderr)
        return None

def restart_frontend_service():
    """Restart the systemd service for the frontend."""
    try:
        subprocess.run(["systemctl", "--user", "restart", FRONTEND_SERVICE], check=True)
        print(f"Restarted {FRONTEND_SERVICE}.")
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error restarting {FRONTEND_SERVICE}: {e}")
        return False

def do_update(host_ip, lan_ips):
    """Update config and restart frontend if the IP changed."""
    new_ip = update_config_host_ip(lan_ips=lan_ips, default=host_ip)
    # Restart frontend if the IP changed.
    if new_ip != host_ip:
        print(f"HOST IP written to a new value ({new_ip}). Restarting frontend.")
        if not restart_frontend_service():
            print(f"Restart failed, restoring original host_ip ({host_ip}) in config file.")
            write_config_ip(host_ip)
            return host_ip
    else:
        print(f"HOST IP unchanged ({host_ip}). Frontend restart skipped.")
    return new_ip

def do_check():
    """Monitor the IP and restart the service if necessary."""
    host_ip = get_config_host()
    # Config HOST IP check.
    if not host_ip:
        print("No valid HOST IP in config.json.")
        return

    lan_ips = get_lan_ips()
    # LAN IPs check.
    if not lan_ips:
        print("Error: No LAN IPs found.", file=sys.stderr)
        return

    # Do check.
    if host_ip.startswith("127") or (host_ip not in lan_ips):
        print(f"HOST IP ({host_ip}) is either local or does not match a LAN IP {lan_ips}. Attempting fix...")
        new_ip = do_update(host_ip, lan_ips)
        print(f"HOST IP ({host_ip}) updated to LAN IP {new_ip}. Frontend server restarted.")
        return True
    else:
        print(f"HOST IP ({host_ip}) matches an external LAN IPs {lan_ips}. No action required.")
        return False

def main():
    try:
        # Your main task logic here
        success = do_check()
        if success:
            print("Success: Task completed.")
        else:
            print("Nothing to do: No action required.")
    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)

if __name__ == "__main__":
    print("Starting task runner...")
    while True:
        main()
        time.sleep(10)  # Wait for 10 seconds before checking again.
