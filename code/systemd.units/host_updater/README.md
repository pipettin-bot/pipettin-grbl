# LAN IP Monitor Script

This script monitors the `config.json` file for the `HOST` IP and compares it with the current LAN IPs of the machine. If the `HOST` IP doesn't match any of the accessible LAN IPs, the script will try to replace it whith a LAN IP, and restart the systemd service of the frontend application.

## What the Script Does

1. **Read `config.json`**: The script reads the `config.json` file and retrieves the current `HOST` IP.
2. **Get LAN IPs**: It checks the current LAN IPs of the machine using `socket` and `netifaces` libraries.
3. **Compare IPs**: If the `HOST` IP in `config.json` doesn't match any of the current LAN IPs (prioritizing external IPs), it proceeds to update the `HOST` field in the `config.json`.
4. **Restart Service**: If the IP address is updated, the script will restart the systemd service of the frontend application.

### Updating Hardcoded Values

If any of the following values change, they should be updated in the script:

1. **`CONFIG_FILE_PATH`**:
   - Path to the `config.json` file that contains the `HOST` key.
   - Default: `"/home/pi/pipettin-bot/code/pipettin-gui/config.json"`

2. **`SYSTEMD_SERVICE_NAME`**:
   - The name of the systemd _user_ service that should be restarted when the IP changes.
   - Default: `"nodegui.service"`

## Systemd Unit

A systemd unit is used to run the Python script as a service. It ensures that the script is continuously monitoring the IP address and taking necessary actions if the IP changes.

The systemd unit (ip-checker.service) runs the script periodically to ensure the config.json file always contains a valid IP.

### Updating Hardcoded Values

If any of the following values change, they should be updated in the unit:

- Location of the main script: `/home/pi/pipettin-bot/code/systemd.units/host_updater/monitor_ip.py`

If edited, the unit should be reloaded and the service restarted (commands below).

### Install the unit

Link the systemd unit file to the user's unit directory:

```bash
ln -s "$(pwd)/monitor_ip.py" ~/.config/systemd/user/
```

Reload, enable, and restart the unit:

```bash
systemctl --user daemon-reload
systemctl --user enable ip-monitor.service
systemctl --user restart ip-monitor.service
journalctl -f --user -u ip-monitor.service  # To monitor messages
```
