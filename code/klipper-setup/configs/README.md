# Klipper configuration files for Pipettin

The "latest" versions are last in this document.

## Zero

First version of the robot using Klipper, using the wooden baseplate and steel rods.

Codename: `pipettin zero`

Base configuration: [pipettin-bot-zero](./pipettin-bot-zero)

## MK2

Second robot using klipper and two CNC-shields. This robot was never completed, and only used one custom pipette, without tool-changing.

Codename: `pipettin minion`

Base configuration: [pipettin-bot-mk2](./pipettin-bot-mk2)

## MK3

Third robot using Klipper, and a single Duet 2 WiFi board. Until extended this robot will only be using a single micropipette (initially an adapter for commercial handheld micropipettes).

Codename: `pipettin oldman`

### Duet2 WiFi

Single-pipette, tool-changing robot.

Base configuration: [pipettin-bot-mk3-duet2](./pipettin-bot-mk3-duet2)

### Duet2 WiFi plus CNC-Shield

Additional tools for a multi-pipette robot:

1. pMULTI tool, using stepper motor from OT's micropipette. Configured on the Duet.
2. pMULTI tool, using a regular NEMA17 motor. Configured on the CNC-Shield.
2. p200 tool adapter, using a regular NEMA17 motor. Configured on the CNC-Shield.

Base configuration: [pipettin-bot-mk3-duet2-cnc_shield](./pipettin-bot-mk3-duet2-cnc_shield)

### 2x CNC-Shield

Multi-pipette robot, as the above, with a pair of CNC-shields.

Base configuration: [pipettin-bot-mk3-2x-cnc_shield](./pipettin-bot-mk3-2x-cnc_shield)

This Has updated homing macros, that skip homing if it is not needed (zeroing instead).

# Pin aliases

Arduino CNC shield: [cnc_shield.cfg](./pin_aliases/cnc_shield.cfg)

Duet 2 WiFi: ?
