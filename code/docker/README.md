# Docker Compose Project for Node.js, Python Services, and MongoDB

This project sets up a multi-container Docker environment for a MERN (MongoDB, Express, React, Node.js) application with additional Python services.

## Project Structure

- [docker-compose.yml](docker-compose.yml): Defines and configures the services (Node.js, MongoDB, and Python services). It also manages networking between containers.
- The configured services are:
  - [node_app's Dockerfile](node_app/Dockerfile): Builds the MERN application, installs dependencies, and runs the web UI.
  - [piper_app's Dockerfile](piper_app/Dockerfile): Installs the robot controller application, and runs it.
  - MongoDB, available to the host on port 28017 (and in the usual port in the container network).

## Starting the Application

To start the entire application stack with Docker Compose, run the following command:

```bash
docker-compose up --build
```

This command will:

1. Build the images for the Node.js backend, frontend, and other services.
2. Start the MongoDB service, Node.js app, and other services.

**Note**: The `--build` flag ensures that the containers are rebuilt if changes are made to the Dockerfiles or application code.

## Accessing the Frontend

Once the application has started, you can access the frontend application via your web browser by navigating to: <http://localhost:3333>

The frontend will be available on port `3333` and the API on `3000` by default. You can configure the port in the `config.json` file or via environment variables in `.env`, by modifying the `docker-compose.yml` file and rebuilding.

## Stopping the Application

To stop the application and remove all containers, use the following command:

```bash
docker-compose down
```

## Additional Configuration

- Several ports and URLs can be adjusted here. Don't change the service names, as `piper` expects only certain values in order to connect.
- Serial devices: sharing serial devices with some services is crucial. See for example the devices in the [docker-compose.yml](docker-compose.yml) for the `piper` service.
