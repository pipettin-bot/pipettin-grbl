# Jupyter Notebooks

Here I'll leave reference notebooks for controlling the robot.

## Playground

Notebooks for fun!

The [gcode-test-lorentz_attractor.ipynb](gcode-test-lorentz_attractor.ipynb) notebook generates GCODE coordinates for the trajectory of the Lorentz Attractor, and saves them to a file. You can run the file on the robot by uploading it to Mainsail.

![elmo_chaos](https://media1.tenor.com/m/z_KoI0-y7rEAAAAC/chaos.gif)
