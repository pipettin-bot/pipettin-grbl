#!/usr/bin/env python3

import os
import configparser
import subprocess

# Read the .git/config file
config = configparser.ConfigParser()
config.read(".gitmodules")

submodules = {}

print("\n=== Parsed Config Sections and Options ===")
for section in config.sections():
    print(f"[{section}]")
    for key, value in config[section].items():
        print(f"  {key} = {value}")

# Iterate over sections and extract submodule paths and URLs
for section in config.sections():
    if section.startswith("submodule "):
        try:
            submodule_path = section.split('"')[1]  # Extract the submodule path
            submodule_url = config[section]["url"]
            submodules[submodule_path] = submodule_url
        except KeyError:
            continue  # Skip invalid submodule entries

# Clone each submodule as an independent repository
for submodule, url in submodules.items():
    os.makedirs(os.path.dirname(submodule), exist_ok=True)

    if not os.path.isdir(os.path.join(submodule, ".git")):
        print(f"Cloning {url} into {submodule}...")
        subprocess.run(["git", "clone", url, submodule], check=True)
    else:
        print(f"Skipping {submodule}, already exists.")
