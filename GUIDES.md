# Guides

The main assembly documentation has moved to StepWiseDocs.com: <https://www.stepwisedocs.com/docs/projects>

All our development documentation has moved to our wiki:

- Browse it online here: [https://docs.openlabautomata.xyz/](https://docs.openlabautomata.xyz/)
- Or directly on GitLab: [https://gitlab.com/pipettin-bot/wiki-js/-/blob/master/home.md](https://gitlab.com/pipettin-bot/wiki-js/-/blob/master/home.md)

More details about each component may be found in this repo, as described in the [SITEMAP.md](./SITEMAP.md).
